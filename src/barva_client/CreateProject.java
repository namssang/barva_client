package barva_client;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class CreateProject extends JDialog{

	Socket socket;
	DataOutputStream dos;
	DataInputStream dis;
	WorkState STATE;
	JLabel laProName, laEndDate, laComment;
	JTextField tfproName, tfEndDate, tfComment;
	JButton btnOk, btnCancel;
	JTextArea taComment;

	CreateProject(Socket socket, final DataOutputStream dos, final DataInputStream dis, WorkState state){
		super(init.frame, "프로젝트 개설", true);
		this.socket = socket;
		this.dos = dos;
		this.dis = dis;
		this.STATE = state;
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);		//x버튼이 먹히지않음.

		try {
			//BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			while(!Common.EnableClick){
				System.out.print("infinite loop..?");
				//click이 가능해지면 빠져나옴.
			}
			dos.writeUTF("CP");               //프로젝트 개설 메세지 알림
			Color dd= new Color(189,189,189);
			Font f1;
			f1 = new Font("견고딕",Font.BOLD,15);
			getContentPane().setBackground(dd);
			setTitle("프로젝트 개설");
			setLayout(null);
			setBackground(Color.BLACK);
			laProName = new JLabel("프로젝트 이름 : ");
			laProName.setForeground(Color.black);
			tfproName = new JTextField(15);
			tfproName.setBackground(Color.white);
			laEndDate = new JLabel("종료일 : ");
			laEndDate.setForeground(Color.black);
			tfEndDate = new JTextField("20160525", 15);
			tfEndDate.setBackground(Color.white);
			laComment = new JLabel("설명 : ");
			laComment.setForeground(Color.black);
			taComment = new JTextArea("");

			taComment.setLineWrap(true);
			taComment.setBackground(Color.white);

			btnOk = new JButton("확인");
			btnOk.setForeground(Color.black);
			btnOk.setContentAreaFilled(false);
			btnOk.setFont(f1);

			btnCancel = new JButton("취소");
			btnCancel.setForeground(Color.black);
			btnCancel.setContentAreaFilled(false);
			btnCancel.setFont(f1);
			laProName.setBounds(10, 10, 100, 20);
			tfproName.setBounds(110, 10, 170, 20);

			laEndDate.setBounds(10, 50, 100, 20);
			tfEndDate.setBounds(110, 50, 170, 20);

			laComment.setBounds(10, 90, 100, 20);
			taComment.setBounds(110, 90, 170, 100);

			btnOk.setBounds(50, 210, 80, 30);
			btnCancel.setBounds(160, 210, 80, 30);

			add(laProName);
			add(tfproName);
			add(laEndDate);
			add(tfEndDate);
			add(laComment);
			add(taComment);
			add(btnOk);
			add(btnCancel);

			btnCancel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						dos.writeUTF("cancel");
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					setVisible(false);

					STATE.setState(true);
				}
			});
			btnOk.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String project_name = tfproName.getText();
					String finDt = tfEndDate.getText();
					String comment = taComment.getText();


					try {
						dos.writeUTF(project_name+"&"+Common.loginId+"&"+finDt+"&"+comment);
						if(dis.readBoolean()){
							FileWriter fw;
							String project_id = dis.readUTF();
							String src = Common.src;
							File desti = new File(src+"/"+Common.loginId+"/"+project_name);
							if(!desti.exists()){
								//없다면 생성
								desti.mkdirs(); 
								src = desti.getPath();

								File f = new File(src+"/"+"info");
								f.createNewFile();
								String comm = "C:\\WINDOWS\\System32\\ATTRIB.EXE +H "+f;
								Runtime.getRuntime().exec(comm);
								fw = new FileWriter(f);
								fw.write(project_id);
								fw.close();
							}

							new CommonDialog("개설 완료");
							setVisible(false);
							System.out.println("1");
							init.pjlist.reload();
						}
						else{
							new CommonDialog("해당 프로젝트는 이미 있습니다.");
							setVisible(false);
						}
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					STATE.setState(true);


				}
			});




			//reader.close();





		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//STATE.setState(true);

		setSize(300,300);
		setLocation(450, 220);
		setVisible(true);
		setResizable(false);
	}

}