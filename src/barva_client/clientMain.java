package barva_client;


import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.AbstractCellEditor;
import javax.swing.Action;
import javax.swing.DefaultCellEditor;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;


public class clientMain{
	static Socket serverSocket;
	static socketClient server;




	public static void main(String[] args) {


		try {
			configureSetting();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		server = new socketClient();               //서버에 연결을 시도한다.
		serverSocket = server.getSocket();                     //연결이 성공하면 소켓을 받아온다.
		new init(server, serverSocket);

		//	new SendingDialog().setVisible(true);;

	}

	public static void configureSetting() throws IOException{
		File f = new File("C:\\");
		if(f.exists()){
			fileSet(f);
		}
		else{
			f = new File("D:\\");
			if(f.exists()){
				fileSet(f);
			}
		}

	}
	public static void fileSet(File f) throws IOException{
		File configFile = new File(f.getPath()+"BarvaConfig");
		if(configFile.exists()){ //설정 파일이 있으면
			@SuppressWarnings("resource")
			FileReader fr = new FileReader(configFile.getPath());
			int ch;
			String path = "";
			while((ch = fr.read())!= -1){
				path += (char)ch;
			}
			Common.src = path;
			Common.configSrc = configFile.getPath();
			Common.path = path;
			fr.close();

		}
		else{//설정파일이 없으면
			configFile.createNewFile();
			String comm = "C:\\WINDOWS\\System32\\ATTRIB.EXE +H "+f;
			Runtime.getRuntime().exec(comm);
			FileWriter fw = new FileWriter(configFile);
			fw.write(System.getProperty("user.dir"));
			fw.close();
			Common.configSrc = configFile.getPath();
			Common.path = Common.src;
		}
	}
}

class init extends JFrame{

	static String src = Common.src;
	static GridBagConstraints c1;
	static GridBagLayout gridbag1;
	static JFrame frame;
	String title;
	static Socket serverSocket;
	static socketClient server;
	static JPanel subPanel, mainPanel, loginPanel, subPanel2, finishPanel;

	JLabel la1 = new JLabel(new ImageIcon(getClass().getClassLoader().getResource("pictures/cloud.png")));
	JLabel la2 = new JLabel(new ImageIcon(getClass().getClassLoader().getResource("pictures/sync.png")));
	JLabel la3 = new JLabel(new ImageIcon(getClass().getClassLoader().getResource("pictures/sync-folder.png")));

	//static JButton btn1 = new JButton("테스트");
	static JButton create = new JButton("  프로젝트 개설");
	static JButton participate = new JButton("프로젝트 참여");
	//static JButton history_test = new JButton("히스토리");
	static JButton detail = new JButton();

	static JLabel detailLabel;
	static String selectProjectNumber;

	static JFrame thisFrame;

	static DynamicTree pjlist;
	static Detail detailTab;
	static SendingDialog sdlg = new SendingDialog();


	public init(final socketClient server, Socket serverSocket) {
		this.server = server;
		this.serverSocket = serverSocket;
		frame = this;

		thisFrame = this;
		detailTab  = new Detail(thisFrame);
		create.setIcon(new ImageIcon(getClass().getClassLoader().getResource("pictures/cre1.png")));
		participate.setIcon(new ImageIcon(getClass().getClassLoader().getResource("pictures/join1.png")));


		setTitle("Barva - 현재 저장 경로 : "+Common.src);
		setSize(600,400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameToCenter(600, 400);

		setLayout(null);

		Color dd= new Color(189,189,189);
		gridbag1 = new GridBagLayout();
		c1 = new GridBagConstraints();

		divFrame();
		//----------------------------------------------------------------
		Font f1;
		f1 = new Font("견고딕",Font.BOLD,13);
		participate.setFont(f1);
		create.setFont(f1);

		//맨위창 패널.
		//		Font f2 = new Font("견고딕",Font.BOLD,20);
		//		Font f3 = new Font("견고딕",Font.ITALIC,18);
		//		JLabel g1 = new JLabel("B");
		//		JLabel g2 = new JLabel("a");
		//		JLabel g3 = new JLabel("r");
		//		JLabel g4 = new JLabel("v");
		//		JLabel g5 = new JLabel("a");
		//		g1.setForeground(Color.BLACK);
		//		g2.setForeground(Color.BLACK);
		//		g3.setForeground(Color.BLACK);
		//		g4.setForeground(Color.BLACK);
		//		g5.setForeground(Color.BLACK);
		//		g1.setFont(f2);
		//		g2.setFont(f2);
		//		g3.setFont(f2);
		//		g4.setFont(f2);
		//		g5.setFont(f2);
		//		JButton g6 = new JButton("");
		//		JButton g7 = new JButton("");
		//		JButton g8 = new JButton("");
		//		JButton g9 = new JButton("");
		//		JButton g10 = new JButton("");
		//		JButton g11 = new JButton("");
		//		JButton g12 = new JButton("");
		//		JButton g13 = new JButton("");
		//		JButton g14 = new JButton("");
		//		JButton g15 = new JButton("");
		//		JButton g16 = new JButton("");
		//		JButton g17 = new JButton("");
		//		JButton g18 = new JButton("");
		//		JButton g19 = new JButton("");
		//		JButton g20 = new JButton("X");

		//로그인 후 프로젝트개설버튼
		create.setForeground(Color.black);
		create.setFocusable(false);
		create.setContentAreaFilled(false);
		create.setFont(f1);
		participate.setForeground(Color.black);
		participate.setFocusable(false);
		participate.setContentAreaFilled(false);
		participate.setFont(f1);

		//		g20.setFont(f3);
		//		g20.addActionListener(new ActionListener() {
		//
		//			@Override
		//			public void actionPerformed(ActionEvent e) {
		//				// TODO Auto-generated method stub
		//				System.exit(0);
		//			}
		//		});
		//
		//		g18.setBorderPainted(false);
		//		g19.setBorderPainted(false);
		//		g20.setBorderPainted(false);
		//		g18.setForeground(Color.lightGray);
		//		g19.setForeground(Color.lightGray);
		//		g20.setForeground(Color.lightGray);
		//		g18.setBackground(Color.darkGray);
		//		g19.setBackground(Color.darkGray);
		//		g20.setBackground(Color.darkGray);
		//		finishPanel.add(g1);
		//		finishPanel.add(g2);
		//		finishPanel.add(g3);
		//		finishPanel.add(g4);
		//		finishPanel.add(g5);
		//		finishPanel.add(g6);
		//		finishPanel.add(g7);
		//		finishPanel.add(g8);
		//		finishPanel.add(g9);
		//		finishPanel.add(g10);
		//		finishPanel.add(g11);
		//		finishPanel.add(g12);
		//		finishPanel.add(g16);
		//		finishPanel.add(g17);
		//		finishPanel.add(g18);
		//		finishPanel.add(g19);
		//		finishPanel.add(g20);
		//		g6.hide();
		//		g7.hide();
		//		g8.hide();
		//		g9.hide();
		//		g10.hide();
		//		g11.hide();
		//		g12.hide();
		//		g16.hide();
		//		g17.hide();
		//		g19.hide();
		//		g18.hide();

		//----------------------------------------------------------
		JButton b1 = new JButton("");
		//		JButton b2 = new JButton("");
		//		JButton b3 = new JButton("");
		JButton config = new JButton("저장 경로 변경",new ImageIcon(getClass().getClassLoader().getResource("pictures/config.png")));	
		JButton home = new JButton("홈");
		config.setForeground(Color.black);
		config.setBackground(Color.DARK_GRAY);
		config.setBorderPainted(false);
		config.setFocusable(false);
		config.setContentAreaFilled(false);

		home.setIcon( new ImageIcon(getClass().getClassLoader().getResource("pictures/home.png")));
		home.setForeground(Color.black);
		home.setBackground(Color.DARK_GRAY);
		home.setBorderPainted(false);
		home.setFocusable(false);
		home.setContentAreaFilled(false);
		home.setFont(f1);
		//	JButton test = new JButton("테스트");
		JButton b8 = new JButton("");
		//		JButton b9 = new JButton("");
		//		JButton b10 = new JButton("");
		JButton login=new JButton("로그인", new ImageIcon(getClass().getClassLoader().getResource("pictures/b2.png")));
		login.setForeground(Color.black);
		login.setBackground(Color.DARK_GRAY);
		login.setBorderPainted(false);
		login.setFocusable(false);
		login.setContentAreaFilled(false);

		login.setFont(f1);
		//setUndecorated(true);

		login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("call Login");
				JButton a = (JButton)e.getSource();
				if(a == login){
					subPanel.setVisible(true);
					mainPanel.setVisible(true);
					server.loginProcess();
				}

			}
		});      

		JButton join=new JButton("회원가입", new ImageIcon(getClass().getClassLoader().getResource("pictures/b1.png")));
		join.setForeground(Color.black);
		join.setBackground(Color.DARK_GRAY);
		join.setBorderPainted(false);
		join.setFocusable(false);
		join.setContentAreaFilled(false);


		join.setFont(f1);

		join.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("call join");
				JButton a = (JButton)e.getSource();
				if(a == join){
					subPanel.setVisible(true);
					mainPanel.setVisible(true);
					server.joinProcess();
				}
			}
		});

		/*	btn1.addActionListener(new ActionListener(){         //버튼1
			public void actionPerformed(ActionEvent e){
				//server.FileSender();
			}
		});*/
		config.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				File f = fileOpenDlg();
				if(!(f == null)){

					Common.src = f.getPath();
					setTitle("Barva - 현재 저장 경로 : "+Common.src);
					File tmp = new File(Common.configSrc);
					if(tmp.exists()){
						try {
							FileWriter fw = new FileWriter(tmp.getPath());
							fw.write(f.getPath());
							fw.close();

						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					else{
						try {
							tmp.createNewFile();
							String comm = "C:\\WINDOWS\\System32\\ATTRIB.EXE +H "+f;
							Runtime.getRuntime().exec(comm);
							FileWriter fw = new FileWriter(tmp);
							fw.write(f.getPath());
							fw.close();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}

				}
			}
		});

		create.addActionListener(new ActionListener(){         //프로젝트 개설
			public void actionPerformed(ActionEvent e){
				if(Common.loginState)
					server.createProject();
				else System.out.println("do login");

			}
		});
		participate.addActionListener(new ActionListener(){         //프로젝트 참여
			public void actionPerformed(ActionEvent e){
				if(Common.loginState)
					server.joinProject();

				else System.out.println("do login");

			}
		});

		home.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				while(!Common.EnableClick){
					//기다렸다가 
					System.out.println("home");
				}

				participate.setVisible(true);
				create.setVisible(true);
				detail.setVisible(false);

				server.ProjectReload();

			}
		});



		subPanel.add(b1);
		//		subPanel.add(b2);
		//		subPanel.add(b3);
		subPanel.add(config);
		subPanel.add(login);
		subPanel.add(join);

		b1.hide();
		//		b2.hide();
		//		b3.hide();
		//b4.hide();

		subPanel.setBackground(dd);

		JButton modify =new JButton("회원수정",new ImageIcon(getClass().getClassLoader().getResource("pictures/b3.png")));
		modify.setForeground(Color.black);
		modify.setBackground(Color.DARK_GRAY);
		modify.setBorderPainted(false);
		modify.setFocusable(false);
		modify.setContentAreaFilled(false);
		modify.setFont(f1);


		modify.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				subPanel.setVisible(false);
				mainPanel.setVisible(false);
				server.MemberModify();
			}
		});


		JButton logout = new JButton("로그아웃",new ImageIcon(getClass().getClassLoader().getResource("pictures/b4.png")));
		logout.setForeground(Color.black);
		logout.setBackground(Color.DARK_GRAY);
		logout.setBorderPainted(false);
		logout.setFocusable(false);
		logout.setContentAreaFilled(false);
		logout.setFont(f1);
		subPanel2.setBackground(dd);

		subPanel2.add(home);
		//subPanel2.add(test);
		//	subPanel2.add(history_test);
		subPanel2.add(b8);
		//		subPanel2.add(b9);
		//		subPanel2.add(b10);
		subPanel2.add(modify);
		subPanel2.add(logout);
		//      b5.hide();
		//      b6.hide();
		/*
		history_test.addActionListener(new ActionListener(){         //버튼1
			public void actionPerformed(ActionEvent e){
				server.FileHistory();
			}
		});

		 */		/*test.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub



				JFileChooser jfc = new JFileChooser();
				jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				jfc.showDialog(thisFrame, null);
				File dir = jfc.getSelectedFile();
				System.out.println(dir!=null?dir.getPath():"");

				String path = dir!=null?dir.getPath():"";

				server.FileSender(path);

				//jt_save_path.setText(dir!=null?dir.getPath():"");


			}
		});
		  */
		logout.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){

				subPanel.setVisible(true);
				subPanel2.setVisible(false);
				mainPanel.setVisible(true);
				loginPanel.setVisible(false);
				participate.setVisible(true);
				create.setVisible(true);
				detail.setVisible(false);
				Common.loginState = false;
				Common.loginId = null;
				WorkState.state = false;
				Common.startWork = false;
				pjlist.clear(); 

			}
		});

		b8.hide();
		//		b9.hide();
		//		b10.hide();

		//mainPanel
		c1.fill = GridBagConstraints.BOTH;
		c1.weightx = 1.0;
		c1.weighty = 1.0;
		c1.insets = new Insets(30, 30, 50, 30);
		layout(mainPanel, la1, 0, 0, 2, 2);

		c1.fill = GridBagConstraints.BOTH;
		c1.weightx = 1.0;
		c1.weighty = 1.0;
		c1.insets = new Insets(30, 30, 10, 30);
		layout(mainPanel, la2, 2, 0, 1, 1);

		c1.fill = GridBagConstraints.BOTH;
		c1.weightx = 1.0;
		c1.weighty = 1.0;
		c1.insets = new Insets(30, 30, 50, 30);
		layout(mainPanel, la3, 2, 1, 1, 1);

		//loginPanel
		c1.fill = GridBagConstraints.BOTH;
		c1.weightx = 2.0;
		c1.weighty = 2.0;
		c1.insets = new Insets(30, 30, 50, 0);
		layout(loginPanel, pjlist, 0, 0, 1, 2);

		c1.fill = GridBagConstraints.BOTH;
		c1.weightx = 1.0;
		c1.weighty = 1.0;
		c1.insets = new Insets(30, 30, 10, 30);
		layout(loginPanel, create, 1, 0, 1, 1);
		//      btn2.setVisible(false);

		c1.fill = GridBagConstraints.BOTH;
		c1.weightx = 1.0;
		c1.weighty = 1.0;
		c1.insets = new Insets(30, 30, 50, 30);
		layout(loginPanel, participate, 1, 1, 1, 1);

		c1.fill = GridBagConstraints.BOTH;
		c1.weightx = 1.0;
		c1.weighty = 1.0;
		c1.insets = new Insets(30, 30, 50, 30);
		layout(loginPanel, detail, 1, 0, 1, 2);

		detail.setVisible(false);

		detail.add(detailTab);
		detail.setBackground(dd);
		detail.setEnabled(false);

		setVisible(true);
		setResizable(false);
	}



	void divFrame(){
		//		finishPanel = new JPanel();
		//		finishPanel.setBounds(0, 0, 800, 20);
		//		finishPanel.setLayout(new GridLayout(1, 17));
		//		add(finishPanel);
		//		finishPanel.setBackground(Color.darkGray);

		Color dd= new Color(189,189,189);
		subPanel = new JPanel();
		subPanel.setBounds(0, 0, 600, 40);
		subPanel.setLayout(new GridLayout(1, 4));
		add(subPanel);

		subPanel2 = new JPanel();
		subPanel2.setBounds(0, 0, 600, 40);
		subPanel2.setLayout(new GridLayout(1, 6));
		add(subPanel2);
		subPanel2.setVisible(false);

		mainPanel = new JPanel();
		mainPanel.setBounds(0, 40, 600, 360);
		mainPanel.setLayout(gridbag1);
		mainPanel.setBackground(dd);
		add(mainPanel);

		loginPanel = new JPanel();
		loginPanel.setBounds(0, 30, 600, 360);
		loginPanel.setLayout(gridbag1);
		loginPanel.setBackground(dd);
		loginPanel.setVisible(false);
		add(loginPanel);

	}




	static void setDetailLabel(String message){
		//String tmp[] = message.split("&");
		detailLabel.setText(message);
	}

	public static void layout(JPanel p, Component obj, int x, int y,int width, int height) {
		c1.gridx = x; // 시작위치 x
		c1.gridy = y; // 시작위치 y
		c1.gridwidth = width; // 컨테이너 너비
		c1.gridheight = height;  // 컨테이너 높이      
		gridbag1.setConstraints(obj, c1);
		p.add(obj);
	}

	void frameToCenter(int x, int y){
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int w = (int) screen.getWidth();  // 모니터 가로 크기
		int h = (int) screen.getHeight(); // 모니터 세로 크기

		// 프레임을 화면 중앙에 배치
		setLocation((w - x) / 2, (h- y)/2);
	}
	private static File fileOpenDlg() 
	{
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new File("C:/test"));

		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY );
		int returnVal = chooser.showOpenDialog(null);
		if(returnVal == JFileChooser.APPROVE_OPTION)  
			//승인하면 선택한 파일을 가져오고 리턴한다.
		{
			File f = chooser.getSelectedFile();
			return f;
		}
		return null;
	}


}

class DynamicTree extends JPanel {


	Socket socket;
	DataOutputStream dos;
	DataInputStream dis;
	WorkState STATE;
	protected DefaultMutableTreeNode rootNode;
	protected DefaultTreeModel treeModel;
	protected JTree tree;
	private Toolkit toolkit = Toolkit.getDefaultToolkit();

	static ArrayList<String> cpn;					//create_project_no
	static ArrayList<String> cph;					//create_project_hash
	static ArrayList<String> cpt;					//create_project_title


	static ArrayList<String> jpn;					//join_project_no
	static ArrayList<String> jph;					//join_project_hash
	static ArrayList<String> jpt;					//join_project_title

	static ArrayList<String> pph;					//perent_project_hash

	Project pr ;

	TreeSelectionListener Listener;

	public DynamicTree(Socket socket, DataOutputStream dos, DataInputStream dis, WorkState state) {
		super(new GridLayout(1, 0));
		this.socket = socket;
		this.dos = dos;
		this.dis = dis;
		this.STATE = state;

		rootNode = new DefaultMutableTreeNode("프로젝트");
		treeModel = new DefaultTreeModel(rootNode);
		treeModel.addTreeModelListener(new MyTreeModelListener());

		tree = new JTree(treeModel);
		//	tree.setEditable(true);
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setShowsRootHandles(true);


		cpn  = new ArrayList<String>();
		cph= new ArrayList<String>();
		cpt= new ArrayList<String>();

		jpn= new ArrayList<String>();
		jph= new ArrayList<String>();
		jpt= new ArrayList<String>();

		pph= new ArrayList<String>();

		setListener();

		JScrollPane scrollPane = new JScrollPane(tree);
		add(scrollPane);

	}
	public void reload(){

		System.out.println("2");
		clear();

		new print_project_list(socket, dos, dis, STATE);

	}

	public void reSelect(){
		int[] row = tree.getSelectionRows();


		System.out.println("??????????? : "+ row[0]);
		tree.setSelectionRow(0);
		tree.setSelectionRow(row[0]);
	}

	public void setListener(){
		Listener = new Selector();
		tree.addTreeSelectionListener(Listener);


	}
	public ArrayList<String> getCPH(){

		ArrayList<String> cph = new ArrayList<String>();
		int depth = rootNode.getChildAt(0).getChildCount();
		for(int i=0; i<depth;i++){
			cph.add(""+rootNode.getChildAt(0).getChildAt(i).hashCode());
		}

		return cph;
	}

	public ArrayList<String> getJPH(){

		ArrayList<String> jph = new ArrayList<String>();
		int depth = rootNode.getChildAt(1).getChildCount();
		for(int i=0; i<depth;i++){
			jph.add(""+rootNode.getChildAt(1).getChildAt(i).hashCode());
		}

		return jph;
	}

	public ArrayList<String> getPPH(){

		ArrayList<String> pph = new ArrayList<String>();

		pph.add(""+rootNode.hashCode());
		pph.add(""+rootNode.getChildAt(0).hashCode());
		pph.add(""+rootNode.getChildAt(1).hashCode());

		return pph;
	}

	/** Remove all nodes except the root node. */
	public void clear() {

		cpn.clear();
		cph.clear();
		cpt.clear();
		jpn.clear();
		jph.clear();
		jpt.clear();
		pph.clear();
		
		Detail.file_id.clear();

		System.out.println("3");
		tree.removeTreeSelectionListener(Listener);
		System.out.println("4");
		rootNode.removeAllChildren();
		System.out.println("5");
		treeModel.reload();
		setListener();

		System.out.println("6");
	}

	/** Remove the currently selected node. */
	public void removeCurrentNode() {
		TreePath currentSelection = tree.getSelectionPath();
		if (currentSelection != null) {
			DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode) (currentSelection
					.getLastPathComponent());
			MutableTreeNode parent = (MutableTreeNode) (currentNode.getParent());
			if (parent != null) {
				treeModel.removeNodeFromParent(currentNode);
				return;
			}
		}

		// Either there was no selection, or the root was selected.
		toolkit.beep();
	}

	/** Add child to the currently selected node. */
	public DefaultMutableTreeNode addObject(Object child) {
		DefaultMutableTreeNode parentNode = null;
		TreePath parentPath = tree.getSelectionPath();

		if (parentPath == null) {
			parentNode = rootNode;
		} else {
			parentNode = (DefaultMutableTreeNode) (parentPath.getLastPathComponent());
		}

		return addObject(parentNode, child, true);
	}

	public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent,
			Object child) {
		return addObject(parent, child, false);
	}

	public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent,
			Object child, boolean shouldBeVisible) {
		DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(child);

		if (parent == null) {
			parent = rootNode;
		}

		// It is key to invoke this on the TreeModel, and NOT DefaultMutableTreeNode
		treeModel.insertNodeInto(childNode, parent, parent.getChildCount());

		// Make sure the user can see the lovely new node.
		if (shouldBeVisible) {
			tree.scrollPathToVisible(new TreePath(childNode.getPath()));
		}
		return childNode;
	}

	class MyTreeModelListener implements TreeModelListener {
		public void treeNodesChanged(TreeModelEvent e) {
			DefaultMutableTreeNode node;
			node = (DefaultMutableTreeNode) (e.getTreePath().getLastPathComponent());

			/*
			 * If the event lists children, then the changed node is the child of the
			 * node we've already gotten. Otherwise, the changed node and the
			 * specified node are the same.
			 */

			int index = e.getChildIndices()[0];


			System.out.println("index : "+index);
			node = (DefaultMutableTreeNode) (node.getChildAt(index));

			System.out.println("The user has finished editing the node.");
			System.out.println("New value: " + node.getUserObject());

		}

		public void treeNodesInserted(TreeModelEvent e) {
		}

		public void treeNodesRemoved(TreeModelEvent e) {
		}

		public void treeStructureChanged(TreeModelEvent e) {
		}





	}
	private class Selector implements TreeSelectionListener {

		Selector(){

		}

		public void valueChanged(TreeSelectionEvent event) {

			while(!Common.EnableClick){
				System.out.print(".");
				//click이 가능해지면 빠져나옴.
			}
			System.out.println("execute???");
			//reSelect();
			String message;
			System.out.println("hash code ? :" +event.getNewLeadSelectionPath().hashCode());
			int hash = event.getNewLeadSelectionPath().hashCode();
			int index ;
			if(pph.indexOf(""+hash) == -1){							//perent project는 이벤트를 거른다.
				index = cph.indexOf(""+hash);
				if(index != -1){							
					//create project인 경우 이벤트
					//System.out.println("click? : " + cpt.get(index));
					STATE.setState(false);
					try {

						dos.writeUTF("GP");
						dis.readUTF();					//메세지 대기
						dos.writeUTF(cpn.get(index));					//프로젝트 정보를 받기 위해 먼저 project Number를 전송.
						//System.out.println("send message : "+cpn.get(index));		
						message = dis.readUTF();   								//해당 프로젝트 정보를 가져욤.
						if(!message.equals("fail")){


							System.out.println("result : " + message);
							dos.writeUTF("continue");
							String tmp[] = message.split("&");


							message = dis.readUTF();   						//file_list 받아옴.
							//System.out.println(message);
							String file_list[] = message.split("&&");
							pr= new Project(tmp);
							//pr.print();
							projectSelectEvent(pr, file_list);
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					STATE.setState(true);


				}
				else{
					index = jph.indexOf(""+hash);						//join project인 경우 이벤트
					//System.out.println("click? : " + jpt.get(index));

					STATE.setState(false);

					try {
						dos.writeUTF("GP");
						dis.readUTF();					//메세지 대기
						dos.writeUTF(jpn.get(index));					//프로젝트 정보를 받기 위해 먼저 project Number를 전송.
						//System.out.println("send message : "+jpn.get(index));		

						message = dis.readUTF();   								//해당 ///////프로젝트 정보를 가져욤.

						if(!message.equals("fail")){
							dos.writeUTF("continue");
							//System.out.println("result : " + message);
							String tmp[] = message.split("&");
							message = dis.readUTF();   						//file_list 받아옴.
							String file_list[] = message.split("&&");
							pr = new Project(tmp);
							//pr.print();
							projectSelectEvent(pr, file_list);
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					STATE.setState(true);
				}

			}

			//= cph.indexOf(""+hash);



			//System.out.println(""+obj.toString());



		}


		void projectSelectEvent(Project pr, String[] file_list){

			init.detailTab.setText(socket, dos, dis, STATE, pr, file_list);				// 프로젝트 목록 클릭시 상세정보를 불러온다.


			init.create.setVisible(false);
			init.participate.setVisible(false);
			init.detail.setVisible(true);
			//      Detail();

		}




	}


}

class Detail extends JTabbedPane{
	JFrame parentFrame;

	Socket socket;
	DataOutputStream dos;
	DataInputStream dis;
	WorkState STATE;
	JLabel dateLabel1 ;
	JLabel dateLabel2 ;
	JLabel memberLabel1 ;
	JLabel memberLabel2 ;
	JLabel textLabel1 ;
	JTextArea textLabel2 ;
	JPanel detailPanel ;
	JPanel datePanel;

	JPanel memberPanel;
	JPanel textPanel;
	JPanel FileList ;
	JPanel btnPanel ;
	JButton manage ;
	JButton huge;
	String project_id;
	String message;
	DefaultTableModel defaultTableModel ;


	int tmp = 0;

	static ArrayList<String> file_id = new ArrayList<String>();
	Detail(JFrame parentFrame){

		this.parentFrame = parentFrame;

		dateLabel1 = new JLabel("기간", JLabel.CENTER);
		dateLabel2 = new JLabel(" ", JLabel.CENTER);
		memberLabel1 = new JLabel("매니저", JLabel.CENTER);
		memberLabel2 = new JLabel(" ", JLabel.CENTER);
		textLabel1 = new JLabel("개요", JLabel.CENTER);
		textLabel2 = new JTextArea("");
		textLabel2.setEditable(false);
		textLabel2.setLineWrap(true);
		


		detailPanel = new JPanel(); //전체 큰 페널
		detailPanel.setBackground(Color.white);

		datePanel=new JPanel();
		datePanel.setBackground(Color.white);
		datePanel.setLayout(null);
		dateLabel1.setBounds(0, 5, 80, 15);
		dateLabel2.setBounds(80, 5, 150, 15);
		datePanel.add(dateLabel1);
		datePanel.add(dateLabel2);

		memberPanel=new JPanel();
		memberPanel.setBackground(Color.white);
		memberPanel.setLayout(null);
		memberLabel1.setBounds(0, 5, 80, 15);
		memberLabel2.setBounds(80, 5, 150, 15);
		memberPanel.add(memberLabel1);
		memberPanel.add(memberLabel2);
		huge = new JButton("+");
		huge.setBounds(210,5,20,30);

		huge.setForeground(Color.BLACK);
		huge.setContentAreaFilled(false);
		
		huge.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				new jTextAreaOutputDialog(init.frame,textLabel2.getText());
			}
		});

		textPanel=new JPanel();
		textPanel.setBackground(Color.white);
		textPanel.setLayout(null);
		textLabel1.setBounds(0, 5, 80, 15);

		JScrollPane sp = new JScrollPane(textLabel2);
		sp.setBounds(80, 5, 130, 20);

		textPanel.add(textLabel1);
		textPanel.add(sp);
		textPanel.add(huge);

		FileList = new JPanel();
		FileList.setBackground(Color.white);
		FileList.setLayout(null);




		JLabel FileLabel = new JLabel("파일목록",JLabel.CENTER);
		FileLabel.setBounds(0, 5, 80, 15);
		JButton execDirectory = new JButton("디렉토리열기");
		execDirectory.setBounds(85,5,130,15);
		execDirectory.setForeground(Color.BLACK);
		execDirectory.setContentAreaFilled(false);

		String columnNames[] =
			{ "파일명" ,""};

		Object rowData[][] =
			{
			};



		defaultTableModel =new DefaultTableModel(rowData, columnNames){
			@Override
			public boolean isCellEditable(int row, int column) {
				if (column >= 0) {
					return false;
				} else {
					return true;
				}
			}
		};

		JTable File = new JTable(defaultTableModel);

		File.setRowHeight(20);
		File.getColumn("파일명").setPreferredWidth(300);
		File.getColumn("").setPreferredWidth(20);





		File.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e){
				if(e.getButton() == 1){
					JTable table = (JTable)e.getSource();
					int index = table.getSelectedRow();

					int column = table.getSelectedColumn();
					if(column == 1){
						//System.out.println("맫럳ㄻ자"+file_id.get(table.getSelectedRow()));
						//System.out.println("1Q2W3E444"+table.getValueAt(table.getSelectedRow(), table.getSelectedColumn()));
						//dos.writeUTF(table.getValueAt(table.getSelectedRow(), table.getSelectedColumn()));
						//String[] historyInfo = dis.readUTF().split("&");
						//                   

						clientMain.server.FileHistory(file_id.get(table.getSelectedRow()),project_id,String.valueOf(table.getValueAt(index, 0)));
				}
				if (e.getClickCount() == 2) {  // 더블클릭

					}
				}
			}      
		});

		JScrollPane List = new JScrollPane(File);
		/*
		for(int i=0;i<10;i++){
			Object [] temporaryObject = { "파일 "+i };
			defaultTableModel.addRow(temporaryObject);
		}
		 */
		List.setBounds(10, 25, 220, 135);
		FileList.add(FileLabel);
		FileList.add(execDirectory);
		FileList.add(List,BorderLayout.CENTER);



		btnPanel = new JPanel();
		btnPanel.setBackground(Color.white);
		manage = new JButton("관리");
		manage.setForeground(Color.BLACK);
		manage.setContentAreaFilled(false);
		btnPanel.setLayout(null);
		btnPanel.add(manage);
		manage.setBounds(75,0,90,15);

		detailPanel.add(datePanel);
		detailPanel.add(memberPanel);
		detailPanel.add(textPanel);
		detailPanel.add(FileList);
		detailPanel.add(btnPanel);

		datePanel.setBounds(0, 0, 300, 20);
		memberPanel.setBounds(0, 20, 300, 20);
		textPanel.setBounds(0, 40, 300, 20);
		FileList.setBounds(0, 60, 300, 165);
		btnPanel.setBounds(0, 225, 300, 15);


		detailPanel.setLayout(null);
		addTab("  프 로 젝 트   상 세  ", detailPanel);

		manage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(e.getSource().equals(manage))
					System.out.println("project_id : "+project_id);
				try {
					while(!Common.EnableClick){
						System.out.print("infinite loop..?");
						//click이 가능해지면 빠져나옴.
					}
					STATE.setState(false);
					dos.writeUTF("AP");                  //메세지를 먼저 보냄
					dos.writeUTF(""+project_id);         //프로젝트 id 보냄
					message = dis.readUTF();
					if(message.equals("false")){
						new CommonDialog("관리자가 아닙니다.");
					}
					else{

						STATE.setState(false);
						//new CommonDialog(message);//이 곳에 승인목록 다이얼로그 실행
						new manageBtn(socket, dos, dis, STATE, parentFrame, message, project_id);

					}
					STATE.setState(true);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});


		execDirectory.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try {
					String src = Common.src;
					File dir = new File(src+"/"+Common.loginId); 
					File[] fileList = dir.listFiles(); 
					File[] file = null;
					FileReader fr;
					int ch;
					String pid = "";

					for(int i = 0 ; i < fileList.length ; i++){
						File directory = fileList[i]; 
						//System.out.println(directory.getPath());
						if(directory.isDirectory()){
							file = new File(directory.getCanonicalPath()).listFiles();
						}
						for(int j=0; j <file.length;j++){
							if(file[j].isFile()){
								// 파일이 있다면 파일 이름 출력

								if(file[j].getName().equals("info")){
									fr = new FileReader(file[j].getPath());
									pid = "";
									while((ch = fr.read())!= -1){
										pid += (char)ch;
									}
									if(project_id.equals(pid)){

										File f = new File(file[j].getParent());



										//MS Windows Only
										Process p= Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " +f.getAbsolutePath());

										// or

										//Process p= Runtime.getRuntime().exec("rundll32 SHELL32.DLL,ShellExec_RunDLL " + 

										// file.getAbsolutePath());


										//Apple Mac Only

										//Process p= Runtime.getRuntime().exec("open " + file.getAbsolutePath());

										p.waitFor();
										System.out.println("Process Done");

									} 
								}
							}
						}
					}
				}

				catch (InterruptedException ex) {
					ex.printStackTrace();
					System.out.println(ex);
				} catch (IOException ex) {
					ex.printStackTrace();
					System.out.println(ex);
				}
			}
		});


	}
	public void setText(Socket socket, DataOutputStream dos, DataInputStream dis, WorkState state, Project pr, String[] file_list){
		this.socket = socket;
		this.dos = dos;
		this.dis = dis;
		this.STATE = state;


		defaultTableModel.setNumRows(0);
		//defaultTableModel.
		memberLabel2.setText(pr.getPm());					//pm 아이디
		dateLabel2.setText(pr.getRegDt() +" - "+pr.getFinDt());		//프로젝트 기간
		textLabel2.setText(pr.getComment()); 					//공지사항
		textLabel2.setCaretPosition(0);					//포커스를 맨 윗줄로
		
		this.project_id = pr.getProject_id();

		if(!file_list[0].equals("null3")){

			for(int i=0;i<file_list.length;i++){
				String tmp[] = file_list[i].split("&");            //file_id, file_name, file_extension
				file_id.add(tmp[0]);
				//Object [] temporaryObject = { tmp[1]+"."+tmp[2] , new ImageIcon((getClass().getClassLoader().getResource("pictures/cloud.png"))) };
				Object [] temporaryObject = { tmp[1]+"."+tmp[2] , "+" };
				defaultTableModel.addRow(temporaryObject);
				System.out.println(temporaryObject[1]);
			}
		}


	}

	public Component getComponent(){
		return this;
	}
}

class jTextAreaOutputDialog extends JDialog{
	
	JTextArea comment;
	JButton confirm;
	
	jTextAreaOutputDialog(JFrame parent, String text){
		super(parent, "설명", true);
		

		setLayout(null);
		
		comment = new JTextArea(text);
		comment.setEditable(false);
		comment.setLineWrap(true);
		confirm = new JButton("확인");
		
		JScrollPane sp = new JScrollPane(comment);
		sp.setBounds(10,10,200,100);
		
		confirm.setBounds(80,120,60,25);
		
		confirm.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				dispose();
			}
		});
		
		add(sp);
		add(confirm);
		
		

		setLocationRelativeTo(null);
		setSize(235,195);
		setVisible(true);
		
		
	}
	
}

class manageBtn extends JDialog{                  
	JFrame parentFrame;

	Socket socket;
	DataOutputStream dos;
	DataInputStream dis;
	WorkState STATE;
	String message;
	String project_id;


	JPanel mainPanel = new JPanel();
	JButton btnNotice, btnApprvDeport,btnOk;   //공지사항, 승인 및 추방

	GridBagConstraints c = new GridBagConstraints();
	GridBagLayout gridbag = new GridBagLayout();

	manageBtn(Socket socket, DataOutputStream dos, DataInputStream dis, WorkState state, JFrame parentFrame, String message, String project_id ){
		super(parentFrame, null, true); 
		this.socket = socket;
		this.dos = dos;
		this.dis = dis;
		this.STATE = state;
		this.parentFrame = parentFrame;
		this.message = message;
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		Color dd= new Color(189,189,189);


		setLayout(null);
		setTitle("관리");

		c.fill = GridBagConstraints.BOTH;

		mainPanel = new JPanel();
		btnNotice = new JButton("공지사항 수정");
		btnApprvDeport = new JButton("승인 및 추방");
		btnOk = new JButton("취 소");
		//	      btnNotice.setContentAreaFilled(false);
		//	      btnApprvDeport.setContentAreaFilled(false);
		//	      btnOk.setContentAreaFilled(false);
		btnNotice.setBackground(Color.lightGray);
		btnApprvDeport.setBackground(Color.lightGray);
		btnOk.setBackground(Color.lightGray);

		btnOk.addActionListener(new ActionListener() {   //취소버튼
			public void actionPerformed(ActionEvent e) {
				try {
					dos.writeUTF("cancel");   //취소를 하게 되면 취소 메세지를 보낸다.
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				dispose();
				STATE.setState(true);
			}
		});

		mainPanel.setLayout(null);
		mainPanel.add(btnNotice);
		mainPanel.add(btnApprvDeport);
		mainPanel.add(btnOk);
		mainPanel.setBackground(dd);
		btnNotice.setBounds(30,30,130,30);
		btnApprvDeport.setBounds(190,30,130,30);
		btnOk.setBounds(135,95,80,30);
		mainPanel.setBounds(0, 0, 400, 200);
		mainPanel.setVisible(true);
		add(mainPanel);

		btnNotice.addActionListener(new ActionListener() {   //공지사항 수정버튼
			public void actionPerformed(ActionEvent e) {
				if(e.getSource().equals(btnNotice)){
					dispose();
					new noticeModify(socket, dos, dis, STATE, message, project_id);
				}

			}

		});

		btnApprvDeport.addActionListener(new ActionListener() {      //승인 및 추방버튼
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("project_id : "+project_id);
				//               dos.writeUTF("AP");                  //메세지를 먼저 보냄
				//               dos.writeUTF(""+project_id);         //프로젝트 id 보냄
				//               message = dis.readUTF();

				dispose();
				new manageProject(socket, dos, dis, STATE, parentFrame, message);

			}

		});

		setSize(370,180);
		frameToCenter(350, 180);
		setVisible(true);


	}

	void frameToCenter(int x, int y){
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int w = (int) screen.getWidth();  // 모니터 가로 크기
		int h = (int) screen.getHeight(); // 모니터 세로 크기

		// 프레임을 화면 중앙에 배치
		setLocation((w - x) / 2, (h- y)/2);
	}
}
class manageProject extends JDialog{						//프로젝트 관리 버튼 눌렀을 시 보여지는 프로젝트 참여 목록, 참여 신청 목록 창
	JFrame parentFrame;

	Socket socket;
	DataOutputStream dos;
	DataInputStream dis;
	WorkState STATE;
	String message;
	JPanel leftPanel = new JPanel();
	JPanel rightPanel= new JPanel();
	JPanel leftSubPanel = new JPanel();
	JPanel rightSubPanel = new JPanel();	

	JScrollPane leftScrollPane ;
	JScrollPane rightScrollPane ;

	//JButton leftSubButton = new JButton("추방");
	//JButton rightSubButton = new JButton("승인");
	JButton closeButton = new JButton("확인");

	ArrayList<JLabel> memberLabel = new ArrayList<JLabel>();
	ArrayList<JLabel> nonMemberLabel = new ArrayList<JLabel>();
	ArrayList<JButton> memberButton = new ArrayList<JButton>();
	ArrayList<JButton> nonMemberButton = new ArrayList<JButton>();

	ArrayList<String> mpn = new ArrayList<String>();
	ArrayList<String> nmpn= new ArrayList<String>();
	ArrayList<String> mpt = new ArrayList<String>();
	ArrayList<String> nmpt = new ArrayList<String>();

	GridBagConstraints c = new GridBagConstraints();
	GridBagLayout gridbag = new GridBagLayout();

	manageProject(Socket socket, DataOutputStream dos, DataInputStream dis, WorkState state, JFrame parentFrame, String message ){
		super(parentFrame, null, true); 
		this.socket = socket;
		this.dos = dos;
		this.dis = dis;
		this.STATE = state;
		this.parentFrame = parentFrame;
		this.message = message;
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		Color dd= new Color(189,189,189);

		setLayout(null);
		setTitle("승인 및 추방");
		getContentPane().setBackground( dd );

		c.fill = GridBagConstraints.BOTH;

		setting();


		//rightScrollPane = new JScrollPane(rightSubPanel);
		//rightScrollPane.setPreferredSize(new Dimension(280,330));	

		leftPanel.add(leftSubPanel);
		leftPanel.setBackground(Color.lightGray);
		rightPanel.add(rightSubPanel);
		rightPanel.setBackground(Color.lightGray);
		closeButton.setForeground(Color.black);
		closeButton.setContentAreaFilled(false);



		closeButton.setBounds(200,285,80,25);	
		closeButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				//System.out.println("exit request!!!");
				try {
					dos.writeUTF("cancel");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				dispose();
				STATE.setState(true);
			}
		});


		add(leftPanel);
		add(rightPanel);
		add(closeButton);

		setSize(500,350);
		frameToCenter(550, 300);
		setVisible(true);


	}

	void frameToCenter(int x, int y){
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int w = (int) screen.getWidth();  // 모니터 가로 크기
		int h = (int) screen.getHeight(); // 모니터 세로 크기

		// 프레임을 화면 중앙에 배치
		setLocation((w - x) / 2, (h- y)/2);
	}
	public void addToPanel(JPanel p, Component com, int x, int y, int width, int height, double weightx, double weighty){
		c.gridx=x;
		c.gridy=y;
		c.gridwidth=width;
		c.gridheight=height;
		c.weightx=weightx;
		c.weighty=weighty;
		p.add(com, c);
	}

	public void reload() throws IOException{


		memberLabel.clear();
		nonMemberLabel.clear();
		memberButton.clear();
		nonMemberButton.clear();

		mpn.clear();
		nmpn.clear();
		mpt.clear();
		nmpt.clear();



		leftSubPanel.removeAll();
		rightSubPanel.removeAll();
		message = dis.readUTF();				//승인목록을 다시 불러온다.

		//System.out.println("reload : "+message);

		setting();

		//	leftSubPanel.repaint();
		leftSubPanel.revalidate();
		//rightSubPanel.repaint();
		rightSubPanel.revalidate();

	}

	public void setting(){

		if(!message.equals("null2")){
			String tmp[] = message.split("&&");
			for(int i=0;i<tmp.length;i++){
				String tmp2[] = tmp[i].split("&");
				if(tmp2[2].equals("1")){
					mpn.add(tmp2[0]);
					mpt.add(tmp2[1]);
				}
				else {
					nmpn.add(tmp2[0]);
					nmpt.add(tmp2[1]);


				}
			}
		}


		leftPanel.setLayout(null);	
		rightPanel.setLayout(null);



		leftPanel.setBounds(15,10,220,270);               //승인목록
		leftPanel.setBackground(Color.lightGray);
		rightPanel.setBounds(250,10,220,270);
		rightPanel.setBackground(Color.lightGray);


		//leftSubPanel.setBounds(10, 10, 280, 330);
		//leftSubPanel.setBackground(Color.cyan);

		leftSubPanel.setLayout(gridbag);

		leftSubPanel.setBounds(10,10,200, mpn.size()*30);
		//		leftSubPanel.setBounds(10,10,280, 300);



		//rightSubPanel.setPreferredSize(new Dimension(280,tmp.length*20));
		//rightSubPanel.setBackground(Color.cyan);
		rightSubPanel.setLayout(gridbag);
		rightSubPanel.setBounds(10,10,200, nmpn.size()*30);
		//		rightSubPanel.setBounds(10,10,280, 300);

		//System.out.println("mpn.size() : " + mpn.size());
		//System.out.println("nmpn.size() : " + nmpn.size());

		if(mpn.size() != 0){
			for(int i=0; i<mpn.size();i++){

				final int index = i;

				memberLabel.add(new JLabel(mpt.get(i),JLabel.CENTER));
				memberButton.add(new JButton("추방"));
				memberButton.get(index).setBackground(Color.LIGHT_GRAY);
				memberButton.get(i).addActionListener(new ActionListener(){			//버튼 각각에 리스너 등록.
					public void actionPerformed(ActionEvent e){
						try {
							dos.writeUTF("kickout");
							dos.writeUTF(mpn.get(index)+"&"+mpt.get(index));

							//remove(memberLabel.get(index));

							reload();

						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				});
				addToPanel(leftSubPanel, memberLabel.get(i) ,0,i,1,1,6.0,1.0);
				addToPanel(leftSubPanel, memberButton.get(i), 1,i,1,1, 1.0, 1.0);

				//memberLabel.get(i).setBackground(Color.cyan);
				//leftSubPanel.add(memberLabel.get(i));
				//leftSubPanel.add(memberButton.get(i));


			}
		}
		else {
			leftSubPanel.setLayout(new FlowLayout());
			leftSubPanel.setBounds(10,10,200, 30);
			leftSubPanel.add(new JLabel("멤버가 없습니다."));
		}

		//-----------------------------

		if(nmpn.size() != 0){
			for(int i=0; i<nmpn.size();i++){
				final int index = i;
				nonMemberLabel.add(new JLabel(nmpt.get(i),JLabel.CENTER));
				nonMemberButton.add(new JButton("승인"));
				nonMemberButton.get(index).setBackground(Color.LIGHT_GRAY);
				nonMemberButton.get(i).addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						try {
							dos.writeUTF("approve");
							dos.writeUTF(nmpn.get(index)+"&"+nmpt.get(index));
							reload();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				});
				addToPanel(rightSubPanel, nonMemberLabel.get(i) ,0,i,1,1, 6.0,1.0);
				addToPanel(rightSubPanel, nonMemberButton.get(i), 1,i,1,1, 1.0, 1.0);

				//nonMemberLabel.get(i).setBackground(Color.cyan);
				//rightSubPanel.add(nonMemberLabel.get(i));
				//rightSubPanel.add(nonMemberButton.get(i));

			}
		}
		else {
			rightSubPanel.setLayout(new FlowLayout());
			rightSubPanel.setBounds(10, 10, 200, 30);
			rightSubPanel.add(new JLabel("신청한 사용자가 없습니다."));
		}




	}
}

class noticeModify extends JDialog{
	Socket socket;
	DataOutputStream dos;
	DataInputStream dis;
	WorkState STATE;
	JLabel lblNotice;
	JTextArea tfnotice;
	JButton btnSubmit, btnCancel;
	String message, notice;
	Project pr ;

	//   public void refresh() throw IOException{
	//         
	//   }
	noticeModify(final Socket socket, final DataOutputStream dos, final DataInputStream dis, final WorkState state, final String message, String project_id){
		super(init.frame, "공지사항 수정", true);
		this.socket = socket;
		this.dos = dos;
		this.dis = dis;
		this.STATE = state;
		this.message = message;

		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		try{
			dos.writeUTF("ModifyNotice");
			Color dd= new Color(189,189,189);


			setTitle("공지사항 수정");
			setBackground(Color.darkGray);
			setLayout(null);


			final String tmp[] = message.split("&");

			Font f1 = new Font("견고딕",Font.BOLD,15);
			getContentPane().setBackground(dd);

			lblNotice = new JLabel("공지사항");
			lblNotice.setBounds(20, 20, 100, 50);
			lblNotice.setForeground(Color.black);

			tfnotice = new JTextArea();

			JScrollPane sp = new JScrollPane(tfnotice);
			sp.setBounds(80, 30, 190, 30);
			sp.setBackground(Color.white);


			btnSubmit = new JButton("수정완료");
			btnSubmit.setBounds(50, 100, 100, 30);
			btnSubmit.setForeground(Color.black);
			btnSubmit.setContentAreaFilled(false);

			btnCancel = new JButton("취 소");
			btnCancel.setBounds(170, 100, 100, 30);
			btnCancel.setForeground(Color.black);
			btnCancel.setContentAreaFilled(false);

			add(lblNotice);
			add(sp);
			//add(tfnotice);
			add(btnSubmit);
			add(btnCancel);



			btnSubmit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					notice = tfnotice.getText();
					if(notice.length() < 1){
						new CommonDialog("공지사항을 입력해 주세요");
					}
					else{
						try {
							dos.writeUTF(project_id+"&"+notice);
							String result = dis.readUTF();

							if(result.equals("sucess")){
								new CommonDialog("공지사항 수정을 완료 하였습니다.");
								init.pjlist.reSelect();

							}
							dispose();
							state.setState(true);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}

				}
			});

			btnCancel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JButton a = (JButton)e.getSource();

					try {
						dos.writeUTF("cancel");
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					dispose();
					state.setState(true);

				}
			});


			setSize(320, 190);
			setLocation(430, 260);
			setResizable(false);
			setVisible(true);   
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}

class ButtonRenderer extends JButton implements TableCellRenderer {

	public ButtonRenderer() {
		setOpaque(true);
	}

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		if (isSelected) {
			setForeground(table.getSelectionForeground());
			setBackground(table.getSelectionBackground());
		} else {
			setForeground(table.getForeground());
			setBackground(UIManager.getColor("Button.background"));
		}
		setText((value == null) ? "" : value.toString());
		return this;
	}
}

/**
 * @version 1.0 11/09/98
 */

class ButtonEditor extends DefaultCellEditor {
	protected JButton button;

	private String label;

	private boolean isPushed;

	public ButtonEditor(JCheckBox checkBox) {
		super(checkBox);
		button = new JButton();
		button.setOpaque(true);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEditingStopped();
			}
		});
	}

	public Component getTableCellEditorComponent(JTable table, Object value,
			boolean isSelected, int row, int column) {
		if (isSelected) {
			button.setForeground(table.getSelectionForeground());
			button.setBackground(table.getSelectionBackground());
		} else {
			button.setForeground(table.getForeground());
			button.setBackground(table.getBackground());
		}
		label = (value == null) ? "" : value.toString();
		button.setText(label);
		isPushed = true;
		return button;
	}

	public Object getCellEditorValue() {
		if (isPushed) {
	
			
			JOptionPane.showMessageDialog(button, label + ": Ouch!");
			// System.out.println(label + ": Ouch!");
		}
		isPushed = false;
		return new String(label);
	}

	public boolean stopCellEditing() {
		isPushed = false;
		return super.stopCellEditing();
	}

	protected void fireEditingStopped() {
		super.fireEditingStopped();
	}
}

