package barva_client;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;


/*===========================================

 회원가입

 ============================================*/


@SuppressWarnings("serial")
public class JoinProcess extends JDialog{                              //회원가입
	Socket socket;
	DataOutputStream dos;
	WorkState STATE;
	JLabel lblId, lblPwd, lblName, lblHp, lblMail, lblDepart ;
	JTextField Jid, Jpassword, Jname, Jphone, Jemail, Jdepart;
	//   JTextField password, name, phone, email, department;
	JButton btnSubmit, btnCancel, idcheck;
	boolean possibleOk;
	String message;


	JoinProcess(Socket socket, final DataOutputStream dos, final DataInputStream dis, final WorkState state){
		super(init.frame,"회원가입",true);
		this.socket = socket;
		this.dos = dos;
		this.STATE = state;

		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);      //x버튼이 먹히지않음.
		while(!Common.EnableClick){
			System.out.print("infinite loop..?");
			//click이 가능해지면 빠져나옴.
		}
		try {
			//BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			dos.writeUTF("JOIN");               //회원가입 메세지 알림

			Color dd= new Color(189,189,189);

			setTitle("회원가입");
			setLayout(null);
			getContentPane().setBackground(dd);

			Font f1;
			f1 = new Font("견고딕",Font.BOLD,15);

			lblId =new JLabel("아이디");
			lblId.setForeground(Color.black);
			lblId.setFont(f1);
			idcheck = new JButton("중복확인");
			idcheck.setForeground(Color.black);
			idcheck.setContentAreaFilled(false);
			lblPwd=new JLabel("패스워드");
			lblPwd.setForeground(Color.black);
			lblPwd.setFont(f1);
			lblName=new JLabel("이름");
			lblName.setForeground(Color.black);
			lblName.setFont(f1);
			lblHp=new JLabel("핸드폰 번호");
			lblHp.setForeground(Color.black);
			lblHp.setFont(f1);
			lblMail=new JLabel("이메일");
			lblMail.setForeground(Color.black);
			lblMail.setFont(f1);
			lblDepart=new JLabel("부서");
			lblDepart.setForeground(Color.black);
			lblDepart.setFont(f1);

			lblId.setBounds(20, 20, 100, 20);
			idcheck.setBounds(290, 20, 90, 20);
			lblPwd.setBounds(20, 50, 100, 20);
			lblName.setBounds(20, 80, 100, 20);
			lblHp.setBounds(20, 110, 100 , 20);
			lblMail.setBounds(20, 140, 100, 20);
			lblDepart.setBounds(20, 170, 100, 20);

			add(lblId);
			add(lblPwd);
			add(lblName);
			add(lblHp);
			add(lblMail);
			add(idcheck);
			add(lblDepart);

			Jid =new JTextField(20);
			Jid.setBackground(Color.white);
			// userId.setText(reader.readLine());
			Jpassword =new JTextField(20);
			Jpassword.setBackground(Color.white);
			//password.setText(reader.readLine());
			Jname= new JTextField(20);
			Jname.setBackground(Color.white);
			// name.setText(reader.readLine());
			Jphone = new JTextField(20);
			Jphone.setBackground(Color.white);
			//phone.setText(reader.readLine());

			Panel panGen = new Panel(new FlowLayout(FlowLayout.LEFT));
			Jemail = new JTextField(20);
			Jemail.setBackground(Color.white);
			//email.setText(reader.readLine());

			Jdepart = new JTextField(20);
			Jdepart.setBackground(Color.lightGray);
			//department.setText(reader.readLine());

			Jemail.setBounds(120, 140, 150, 20);
			Jemail.setBackground(Color.white);
			Jdepart.setBounds(120, 170, 150, 20);
			Jdepart.setBackground(Color.white);

			btnCancel = new JButton("취  소");
			btnCancel.setForeground(Color.black);
			btnCancel.setContentAreaFilled(false);
			btnSubmit = new JButton("가입완료");
			btnSubmit.setForeground(Color.black);
			btnSubmit.setContentAreaFilled(false);

			Jid.setBounds(120, 20, 150, 20);
			Jpassword.setBounds(120, 50, 150, 20);
			Jname.setBounds(120, 80, 150, 20);
			Jphone.setBounds(120, 110, 150, 20);

			Panel paButton = new Panel();
			paButton.add(btnSubmit);
			paButton.add(btnCancel);
			paButton.setBounds(0, 220, 370, 200);

			add(Jid);
			add(Jpassword);
			add(Jname);
			add(Jphone);
			add(Jemail);
			add(Jdepart);
			add(paButton);

			btnCancel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						dos.writeUTF("cancel");                  //취소를 하게 되면 취소 메세지를 보낸다.
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					dispose();
				}
			});

			//중복확인 
			idcheck.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					//System.out.println("idcheck");

					String userId = Jid.getText();

					if(userId.length()<1){
						new CommonDialog("아이디를 입력해주세요");
					}

					else{
						try {
							System.out.println(userId+"&"+userId);
							dos.writeUTF("idcheck"+"&"+userId);

							if(dis.readBoolean()){
								new CommonDialog("중복된 아이디 입니다.");
							}
							else{
								new CommonDialog("사용가능한 아이디 입니다.");
								Jid.setEnabled(false);
								possibleOk = true;
							}
							state.setState(false);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			});


			btnSubmit.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					// System.out.println("btnSubmit");
					String userId = Jid.getText();
					String password = Jpassword.getText();
					String name = Jname.getText();
					String phone = Jphone.getText();
					String email = Jemail.getText();
					String department = Jdepart.getText();

					try {
						//while(equals(possibleOk)==true){
						//  System.out.println(userId+"&"+password+"&"+name+"&"+phone+"&"+email+"&"+department);

						if(possibleOk==true){
							dos.writeUTF(userId+"&"+password+"&"+name+"&"+phone+"&"+email+"&"+department);
							message = dis.readUTF();
						}
						else
							new CommonDialog("아이디 중복체크를 해주세요");

					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					if(message.equals("success")){
						new CommonDialog("회원가입이 완료되었습니다.");
						dispose();
						//setVisible(true);
					}
					else
						setVisible(true);
				}
			});



			setSize(410, 300);
			setLocation(400, 250);
			setResizable(false);
			setVisible(true);    



		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}