package barva_client;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

public class FileHistory extends JDialog{
	Socket socket;
	DataOutputStream dos;
	DataInputStream dis;
	WorkState STATE;
	JLabel laFileName, laModifyTime;   //파일명, 수정날짜, Comment
	JTextArea laComment;
	JButton modify_comment;
	JList ltHistory;   //History리스트
	JButton btnClose;   //닫기 버튼
	JTable jTable;
	int tmp = 0;
	DefaultTableModel defaultTableModel ;
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat file_df = new SimpleDateFormat("yyyyMMddHHmmss");				//파일이름 뒤에 추가할 날짜시간
	
	boolean modify = false;
	JDialog nowDialog;


	//JCheckBox confirm;

	FileHistory(Socket socket, DataOutputStream dos, DataInputStream dis, WorkState state, String file_id, String project_id, String file_name){
		this.socket = socket;
		this.dos = dos;
		this.dis = dis;
		this.STATE = state;
		nowDialog = this;
		
		while(!Common.EnableClick){
			System.out.print("infinite loop..?");
			//click이 가능해지면 빠져나옴.
		}
		try {

			dos.writeUTF("FH");


			setTitle("프로젝트 상세정보");
			setLayout(null);
			String fi = file_id;
			System.out.println("111111 "+fi);
			dos.writeUTF(fi);

			String[] historyInfo = dis.readUTF().split("&");

			String[] filehistoryInfo = dis.readUTF().split("&");
			for(int i =0 ; i<historyInfo.length; i++ ){
				//System.out.println("222222  : "+historyInfo[i]);
			}

			laFileName = new JLabel("파일 이름 : "+filehistoryInfo[0]);
			laFileName.setBounds(30, 20, 250, 30);
			
			

			//laModifyTime = new JLabel("상세정보 : 상세정보상세정보");
			//laModifyTime.setBounds(30, 60, 250, 30);

			laComment = new JTextArea("파일 설명 : "+filehistoryInfo[1]);
			laComment.setEditable(false);
			laComment.setLineWrap(true);

			JScrollPane commentScroll = new JScrollPane(laComment);
			commentScroll.setBounds(30, 50, 250, 80);
			
			modify_comment = new JButton("수정");
			modify_comment.setForeground(Color.BLACK);
			modify_comment.setContentAreaFilled(false);
			modify_comment.setBounds(290,50,80,30);
			
			modify_comment.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					if(modify){
						modify = false;
						laComment.setEditable(false);
						modify_comment.setText("수정");
						String str = laComment.getText();
						laComment.setText("파일 설명 : "+str);
						
						
						try {
							dos.writeUTF("MC1");
							dis.readUTF();
							dos.writeUTF(fi + "&" + str);
							
							//str.
							
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						state.setState(true);
						
					}
					else
					{
						
						modify = true;
						String str = laComment.getText();
						laComment.setText(str.replace("파일 설명 : ", ""));
						laComment.setEditable(true);
						laComment.requestFocus();
						laComment.setCaretPosition(laComment.getDocument().getLength()); 
						modify_comment.setText("확인");
						
						
						
						state.setState(false);
					}
					
				}
			});



			//Object rowData[][] = new rowData[historyInfo.length][3	];

			String columnNames[] = {"수정일", "USER", "변경내용(더블클릭시 수정)", ""};

			Object[][] rowData = new Object[historyInfo.length/3][4];

			for(int i = 0; i < historyInfo.length/3; i++ ){
				for(int j = 0; j < 4; j ++){
					if(j == 0 ){
						try {
							Date n = df.parse(historyInfo[tmp]);

							rowData[i][j] = df.format(n);
							tmp++;
						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					else if(j == 3){
						rowData[i][j] = "+";
					}
					else{
						rowData [i][j] = historyInfo[tmp];
						tmp++;
					}
				}
			}

			defaultTableModel =new DefaultTableModel(rowData, columnNames){
				@Override
				public boolean isCellEditable(int row, int column) {
					if (row >= 0) {
						return false;
					} else {
						return true;
					}
				}
			};

			jTable = new JTable(defaultTableModel);
			jTable.setRowHeight(20);



			jTable.getColumn("수정일").setPreferredWidth(130);
			jTable.getColumn("USER").setPreferredWidth(50);
			jTable.getColumn("변경내용(더블클릭시 수정)").setPreferredWidth(150);
			jTable.getColumn("").setPreferredWidth(20);

			jTable.addMouseListener(new MouseAdapter() {

				public void mouseClicked(MouseEvent e){
					JTable table = (JTable)e.getSource();
					int index = table.getSelectedRow();
					int column = table.getSelectedColumn();
					if(e.getButton() == 1){

						if(column == 3){
							//System.out.println(table.getValueAt(index,0));


							while(!Common.EnableClick){
								System.out.print("????");	
							}

							state.setState(false);

							try {
								dos.writeUTF("SD");				//파일전송요청
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}

							new FileHistory_ReceiveData(socket, dis, dos, project_id, file_name, String.valueOf(table.getValueAt(index , 0)), String.valueOf(table.getValueAt(index ,  1)));
							// 더블클릭시 다운로드 후 파일 열기


							state.setState(true);
						}

					}
					if (e.getClickCount() == 2) {  // 더블클릭
						
						String userid = (String)table.getValueAt(index, 1);
						System.out.println("user id : "+userid);
						if(column == 2 && userid.equals(Common.loginId)){
							state.setState(false);
							String modify_time = (String)table.getValueAt(index, 0);
							String modify_userid = (String)table.getValueAt(index, 1);
							ModifyDialog tmp = new ModifyDialog(socket, dos, dis, nowDialog, fi, modify_time, modify_userid, (String)table.getValueAt(index, column));
							table.setValueAt((String)tmp.getText(), index, column);
							state.setState(true);
						}
						else if(column == 2){
							new CommonDialog("변경한 사용자만 수정 가능합니다.");
						}
					}
				}      
			});
			JScrollPane jScrollPane = new JScrollPane(jTable);
			jScrollPane.setBounds(30, 150, 330, 250);

			btnClose = new JButton("닫기");
			btnClose.setForeground(Color.BLACK);
			btnClose.setContentAreaFilled(false);
			btnClose.setBounds(160, 420, 80, 30);


			add(laFileName);
			//add(laModifyTime);
			add(jScrollPane);
			add(commentScroll);
			add(modify_comment);
			add(btnClose);

			btnClose.addActionListener(new ActionListener() {   //추방버튼 리스너
				public void actionPerformed(ActionEvent e) {
					JButton a = (JButton)e.getSource();
					if(a == btnClose){
						dispose();
					}
				}
			});

			setSize(400, 500);
			setLocation(500, 300);
			setResizable(false);
			setVisible(true);

		} catch(IOException e) {

		}
	}
}

class ModifyDialog extends JDialog{
	JLabel label;
	JTextArea comment;
	JButton cancel, confirm;
	String confirm_text = "";
	
//	ModifyDialog(){
	ModifyDialog(Socket socket, DataOutputStream dos, DataInputStream dis, JDialog parent, String file_id, String modify_time, String modify_userid, String text){
		super(parent,"",true);

	
		setLayout(null);
		String message = null;
		label = new JLabel("변경 내용  ");
		comment = new JTextArea("");
		comment.setLineWrap(true);
		comment.setText(text);
		comment.setCaretPosition(comment.getDocument().getLength()); 
		JScrollPane commentScroll = new JScrollPane(comment);
		label.setBounds(10,10,230,30);
		commentScroll.setBounds(10, 40, 230, 100);

		try {
			dos.writeUTF("MC2");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		cancel = new JButton("취소");
		cancel.setForeground(Color.BLACK);
		cancel.setContentAreaFilled(false);
		cancel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				confirm_text = text;
				try {
					dos.writeUTF("cancel");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				dispose();
			}
		});
		
		
		confirm = new JButton("변경");
		confirm.setForeground(Color.BLACK);
		confirm.setContentAreaFilled(false);
		confirm.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				confirm_text = comment.getText();
				if(confirm_text.equals("")){
					confirm_text = "없음";
				}
				try {
					dos.writeUTF(file_id+"&"+modify_time+"&"+modify_userid+"&"+confirm_text);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				dispose();
			}
		});

		cancel.setBounds(250,40,80,40);
		confirm.setBounds(250,100,80,40);
		

		add(label);
		add(commentScroll);
		add(cancel);
		add(confirm);

		setLocationRelativeTo(null);
		setSize(355,200);
		setVisible(true);
	}
	
	public String getText(){
		
		return confirm_text;
	}
}