package barva_client;

import java.awt.Color;
import java.awt.Font;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;


public class JoinProject extends JDialog{
	Socket socket;
	DataOutputStream dos;
	DataInputStream dis;
	WorkState STATE;
	BufferedReader reader;
	String message;
	int i;
	JLabel laPmId;
	JTextField tfPmId;
	JButton btnSearch, btnOk, btnCancel;
	boolean possibleOk =false;

	ArrayList<String> proNumber = new ArrayList<String>();
	ArrayList<String> proTitle = new ArrayList<String>();


	JoinProject(Socket socket, final DataOutputStream dos, final DataInputStream dis, WorkState state){
		super(init.frame, "프로젝트 참여", true);
		this.socket = socket;
		this.dos = dos;
		this.dis = dis;
		this.STATE = state;
		reader  = new BufferedReader(new InputStreamReader(System.in));
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);		//x버튼이 먹히지않음.
		//	setUndecorated(true);					//타이틀바가 아예 사라짐.
		//setDefaultLookAndFeelDecorated(true);		// 창 디자인이 바뀜..?



		try {
			dos.writeUTF("JP");	

			Color dd= new Color(189,189,189);
			Font f1;
			f1 = new Font("견고딕",Font.BOLD,15);
			getContentPane().setBackground(dd);
			setTitle("프로젝트 참여");
			setLayout(null);
			setBackground(Color.darkGray);

			laPmId = new JLabel("팀장 아이디 입력 : ");
			laPmId.setBounds(10, 10, 110, 30);
			//	         laPmId.setFont(f1);
			laPmId.setForeground(Color.black);

			tfPmId = new JTextField(15);
			tfPmId.setBounds(120, 10, 90, 30);
			tfPmId.setBackground(Color.white);

			btnSearch = new JButton("검색");
			btnSearch.setBounds(215, 10, 65, 30);
			btnSearch.setForeground(Color.black);
			btnSearch.setContentAreaFilled(false);
			btnSearch.setFont(f1);

			//            JPanel pp = new JPanel();
			//            pp.setBounds(30, 60, 300, 200);

			List list = new List();
			list.setBounds(5, 50, 275, 160);
			//list.add("");
			//list.add("");
			add(list);



			btnOk = new JButton("확인");
			btnOk.setBounds(55, 220, 70, 30);
			btnOk.setForeground(Color.black);
			btnOk.setContentAreaFilled(false);
			btnOk.setFont(f1);

			btnCancel = new JButton("취소");
			btnCancel.setBounds(155, 220, 70, 30);
			btnCancel.setForeground(Color.black);
			btnCancel.setContentAreaFilled(false);
			btnCancel.setFont(f1);
			add(laPmId);
			add(tfPmId);
			add(btnSearch);
			//add(pp);


			add(btnOk);
			add(btnCancel);



			list.addItemListener(new ItemListener() {

				public void itemStateChanged(ItemEvent e) {
					System.out.println(list.getSelectedIndex());
					possibleOk = true;

				}
			});
			
			list.setBackground(Color.lightGray);
			btnOk.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					if(possibleOk){
						try {
							dos.writeUTF("ok");
							if(dis.readUTF().equals("ok")){
								dos.writeUTF(proNumber.get(list.getSelectedIndex()));
								if(dis.readBoolean()){
									new CommonDialog("신청하였습니다.");
									dispose();
									STATE.setState(true);
								}
								else{
									new CommonDialog("이미 신청하였습니다.");
								}
							}
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					else{
						new CommonDialog("프로젝트를 선택하세요.");
					}
				}
			});	

			btnCancel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					try {
						dos.writeUTF("cancel");
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					setVisible(false);

					STATE.setState(true);
				}
			});

			btnSearch.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub

					proNumber.clear();
					proTitle.clear();
					list.removeAll();
					String leaderID = tfPmId.getText();
					//int index;
					try {
						dos.writeUTF(leaderID);
						message = dis.readUTF();
						if(!message.equals("null1")){
							if(!message.equals("null2")){
								String result[] = message.split("&&");
								for( i=0; i<result.length;i++){
									//System.out.println((i+1) +"."+result[i]);
									String tmp[] = result[i].split("&");
									proNumber.add(tmp[0]);
									proTitle.add(tmp[1]);

									list.add((i+1)+". "+proTitle.get(i));

								}



							}
							else{
								new CommonDialog("개설된 프로젝트가 없습니다.");
							}
						}
						else{
							new CommonDialog("개설자 본인은 참여할 수 없습니다.");
						}
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			});


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		setSize(300,300);
		setLocation(400, 220);
		setVisible(true);
		setResizable(false);
	}


}
