package barva_client;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextArea;


/*===========================================

 주기적으로 체크 메세지를 교환, 업데이트 사항 확인 작업, 일정 시간 후 자동 로그아웃기능

 ============================================*/


public class CheckMessage extends Thread{
	Socket socket;
	DataOutputStream dos;
	DataInputStream dis;
	WorkState state;
	String src = Common.src;
	FileReader fr;
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat file_df = new SimpleDateFormat("yyyyMMddHHmmss");				//파일이름 뒤에 추가할 날짜시간





	CheckMessage(Socket socket, DataOutputStream dos, DataInputStream dis, WorkState state){
		this.socket = socket;
		this.state = state;
		this.dos = dos;
		this.dis = dis;

	}

	public void run() {
		try {
			while(true){
				if(state.getState()){

					Common.EnableClick = false;
					this.dos.writeUTF("CM");
					//------------------------------------------------


					subDirListForUpload(src+"/"+Common.loginId, dos, dis, state);
					dos.writeUTF("end");
					if(Common.startWork){
						dos.writeUTF("nopass");
						String tmp = "";
						tmp = subDirListSearch(src+"/"+Common.loginId, dos, dis, state);
						System.out.println("directory structure : " +tmp);
						if(tmp.equals("")){
							dos.writeUTF("none");
						}
						else{
							dos.writeUTF(tmp);
						}

						tmp = dis.readUTF();

						if(!tmp.equals("equals")){
							System.out.println("receive file message : "+tmp);
							new RealTimeDialog(tmp);
							new ReceiveStart(socket, dis, dos, tmp);
							new CommonDialog("동기화를 완료했습니다.");

						}
						else{
							System.out.println("file structure is same");
						}

					}
					else{
						dos.writeUTF("pass");
					}

					if(!Common.startWork){
						sleep(1000);
						Common.startWork = true;				//업데이트 된 이후 데이터 받아오기 허용
					}
					Common.EnableClick = true;
					//------------------------------------------------

				}


				sleep(3000);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//System.out.println("서버와 연결이 끊어졌습니다.\n프로그램을 종료합니다.");
			new CommonDialog("서버와 연결이 끊어졌습니다. 프로그램을 종료합니다.");
			System.exit(1);
		}

	}

	public void subDirListForUpload(String source, DataOutputStream dos, DataInputStream dis, WorkState state){
		File dir = new File(source); 
		String project_id;
		File[] fileList = dir.listFiles(); 
		try{
			//dis.readUTF();				//continue 메세지를 받으면

			for(int i = 0 ; i < fileList.length ; i++){
				File file = fileList[i]; 

				if(file.isFile()){
					// 파일이 있다면 파일 이름 출력
					if(!file.getName().equals("info")){
						if(file.getName().indexOf("~$") != 0){
							//System.out.println("Perent Folder : "+ new File(file.getParent()).getName());
							fr = new FileReader(file.getParent()+"/"+"info");
							int ch;
							project_id = "";
							while((ch = fr.read())!= -1){
								project_id += (char)ch;
							}
							String tempfn[] = file.getName().split("[.]");


							if(tempfn.length >= 3){
								dos.writeUTF("cancel");
								new CommonDialog("<"+file.getName()+">"+"파일명에는 .가 포함될 수 없습니다.");
							}
							else{

								dos.writeUTF(project_id + "&" + tempfn[0] + "&" + df.format(new Date(file.lastModified()))+
										"&" + file.length() + "&" + tempfn[1] + "&" + "없음");

								if(dis.readUTF().equals("ok")){									//보내달라는 요청을 받으면 
									init.sdlg.sendProcess(socket, dos, dis, state, project_id, file.getPath(), file_df.format(new Date(file.lastModified())),file.getName());			//파일을 전송한다.
								}

								//////////////////////////////////////////////





								//
								///System.out.println("\t Project ID = "+project_id);
								//System.out.println("\t File Name = " + file.getName());
								//System.out.println("\t File date = " + df.format(new Date(file.lastModified())));
							}



						}


					}
				}else if(file.isDirectory()){
					// 서브디렉토리가 존재하면 재귀적 방법으로 다시 탐색
					subDirListForUpload(file.getCanonicalPath().toString(),dos, dis,state); 
				}

				//System.out.println("file count : "+fileList.length);


			}

		}catch(IOException e){

		}
	}

	public int getFileCount(String source){
		File dir = new File(source); 
		String project_id;
		File[] fileList = dir.listFiles(); 
		int file_count = 0;

		try{
			//dis.readUTF();				//continue 메세지를 받으면

			for(int i = 0 ; i < fileList.length ; i++){
				File file = fileList[i]; 

				if(file.isFile()){
					// 파일이 있다면 파일 이름 출력
					if(!file.getName().equals("info")){
						file_count += 1;
					}
				}else if(file.isDirectory()){
					// 서브디렉토리가 존재하면 재귀적 방법으로 다시 탐색
					file_count += getFileCount(file.getCanonicalPath().toString()); 
				}
			}


		}catch(IOException e){

		}
		return file_count;
	}



	public String subDirListSearch(String source, DataOutputStream dos, DataInputStream dis, WorkState state){

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat file_df = new SimpleDateFormat("yyyyMMddHHmmss");				//파일이름 뒤에 추가할 날짜시간
		File dir = new File(source); 
		String project_id = "";
		File[] fileList = dir.listFiles(); 
		String file_info = "";

		try{
			//dis.readUTF();				//continue 메세지를 받으면




			for(int i = 0 ; i < fileList.length ; i++){
				File file = fileList[i]; 

				if(file.isFile()){
					if(project_id.equals("")){
						fr = new FileReader(file.getParent()+"/"+"info");
						int ch;
						while((ch = fr.read())!= -1){
							project_id += (char)ch;
						}
					}

					if(!file.getName().equals("info")){

						String tempfn[] = file.getName().split("[.]");

						String time = df.format(new Date(file.lastModified()));

						file_info += project_id + "&" + tempfn[0] + "&" + time +"&&";



					}

					//System.out.println("Perent Folder : "+ new File(file.getParent()).getName());


				}else if(file.isDirectory()){
					// 서브디렉토리가 존재하면 재귀적 방법으로 다시 탐색
					file_info += subDirListSearch(file.getCanonicalPath().toString(),dos, dis,state); 
				}

			}

		}catch(IOException e){

		}

		return file_info;
	}

}

class RealTimeDialog extends JDialog{

	RealTimeDialog(String file_info){
		super(init.frame,"알림",true);

		setLayout(null);

		JTextArea text = new JTextArea();
		text.setEditable(false);

		JLabel message = new JLabel ("위 파일이 업데이트 되었습니다. 확인을 누르면 동기화합니다.");
		JButton YES = new JButton("확인");


		String inputText = "";
		String[] tmp1 = file_info.split("&&");

		for(int i=0; i<tmp1.length;i++){
			String[] tmp2 = tmp1[i].split("&");
			inputText += (i+1)+". 프로젝트 이름 : "+tmp2[3]+"\t\t파일 이름 :"+tmp2[1]+"."+tmp2[4]+"\n";



		}


		text.setText(inputText);






		text.setBounds(10,10,460,100);
		message.setBounds(10,110,400,20);
		YES.setBounds(180,130,100,30);

		YES.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				dispose();
			}
		});


		add(text);
		add(message);
		add(YES);

		setLocationRelativeTo(null);
		setSize(500,220);
		setVisible(true);

	}
}


class SendingDialog extends JDialog{
	JLabel label;
	JLabel Lfile_name;
	JLabel percent;
	SendingDialog(){
		super(init.frame,"");

		setUndecorated(true);
		setLayout(null);
		String message = null;
		label = new JLabel("파일을 전송하는 중입니다.");
		Lfile_name = new JLabel("filename");
		percent = new JLabel ("0%");
		label.setBounds(10,20,230,50);
		Lfile_name.setBounds(70, 10, 230, 30);
		percent.setBounds(250,20,50,50);
		ImageIcon gg = new ImageIcon(getClass().getClassLoader().getResource("pictures/ani.gif"));
		Image im = gg.getImage();
		Image img = im.getScaledInstance(50, 50, java.awt.Image.SCALE_DEFAULT);
		ImageIcon ss = new ImageIcon(img);

		label.setIcon(ss);
		add(Lfile_name);
		add(label);
		add(percent);

		setLocationRelativeTo(null);
		setSize(300,80);
	}

	public void sendProcess(Socket socket, DataOutputStream dos, DataInputStream dis, WorkState state, String project_id, String path, String modify_time, String file_name){
		init.frame.setVisible(false);
		setVisible(true);
		Lfile_name.setText("[ "+ file_name +" ]");
		label.setText(" 파일을 전송하는 중입니다.");
		new FileSender(socket, dos, dis, state, project_id, path, modify_time, percent);
		setVisible(false);
		init.frame.setVisible(true);

	}

	public void startReceiveProcess(){
		init.frame.setVisible(false);
		setVisible(true);
		label.setText("프로젝트 파일을 받는 중입니다.");




	}

	public void setLabelText(String str){
		System.out.print(".");
		percent.setText(str+"%");
	}

	public void endReceiveProcess(){

		setVisible(false);
		init.frame.setVisible(true);

	}



}





class FileSender {				//파일 전송 클래스
	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	FileInputStream fis;
	BufferedInputStream bis;
	WorkState STATE;
	BufferedReader reader;


	public FileSender(Socket socket, DataOutputStream dos, DataInputStream dis, WorkState state, String project_id, String path, String modify_time, JLabel label) {
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;
		this.STATE = state;
		reader  = new BufferedReader(new InputStreamReader(System.in));

		try {
			System.out.println("start file send process.");

			// 파일 이름 전송

			File f = new File(path);

			//System.out.print("프로젝트 이름 입력 ");
			String projectId = project_id;
			String tmp[] = f.getName().split("[.]");

			dos.writeUTF(projectId+"&"+tmp[0]+"[T]"+modify_time+"[N]"+Common.loginId+"."+tmp[1]);




			String s =Common.src;





			fis = new FileInputStream(f);
			bis = new BufferedInputStream(fis);
			dos.writeInt((int)f.length());

			double percent = 0;
			String percentStr = null;



			int len;
			int size = 4096;
			byte[] data = new byte[size];
			while ((len = bis.read(data)) != -1) {
				dos.write(data, 0, len);
				percent += len;
				percentStr = String.valueOf((int)(percent / f.length() * 100));
				label.setText(percentStr+"%");
				System.out.print("#");
			}

			dis.readUTF();

			System.out.println("complete.");
			System.out.println("send file size : " + f.length());

			//dos.close();
			dos.flush();
			bis.close();
			fis.close();

			//reader.close();


		} catch (IOException e) {
			e.printStackTrace();
		}


	}
}

class ReceiveData{
	Socket socket;
	DataInputStream dis;
	FileOutputStream fos = null;
	BufferedOutputStream bos = null;
	String src;
	FileReader fr;

	JLabel percentLabel ;
	ReceiveData(Socket socket, DataInputStream dis, DataOutputStream dos){
		this.socket = socket;
		this.dis = dis;




		src = Common.src;
		subDirList(src, dis, dos);
		init.frame.setVisible(true);
		// 바이트 데이터를 전송받으면서 기록

		//bos.write(data, 0, len);


		//dis.close();
		System.out.println("file receive complete");

	}
	public void subDirList(String source, DataInputStream dis, DataOutputStream dos){
		File dir = new File(src+"/"+Common.loginId); 
		SimpleDateFormat dt = new SimpleDateFormat("yyyyMMddHHmmss");
		long mtime = 0;
		String project_id = null;
		int ch=0;
		File[] fileList = dir.listFiles(); 
		File[] file = null;
		int cnt = 0;
		try{	
			while(true){
				init.frame.setVisible(false);
				//init.sdlg.setVisible(true);
				String message = dis.readUTF();									//project_id, filename, file_modify_time
				String tmp[] = message.split("&");
				if(!message.equals("end")){
					cnt++;
					//System.out.println(fileList.length);
					for(int i = 0 ; i < fileList.length ; i++){
						File directory = fileList[i]; 
						//System.out.println(directory.getPath());
						if(directory.isDirectory()){
							file = new File(directory.getCanonicalPath()).listFiles();
						}
						for(int j=0; j <file.length;j++){
							if(file[j].isFile()){
								// 파일이 있다면 파일 이름 출력

								if(file[j].getName().equals("info")){
									fr = new FileReader(file[j].getPath());
									project_id = "";
									while((ch = fr.read())!= -1){
										project_id += (char)ch;
									}
									if(tmp[0].equals(project_id)){
										File f = new File(file[j].getParent()+"/"+tmp[1]);
										if(f.exists()){
											String dt1 = dt.format(new Date(f.lastModified()));
											String dt2 = dt.format(new Date(dt.parse(tmp[2]).getTime()));
											if(dt1.compareTo(dt2) != 0){
												System.out.println(new Date(f.lastModified()));
												System.out.println(new Date(dt.parse(tmp[2]).getTime()));
												//System.out.println(f.getPath());
												init.frame.setVisible(false);
												//init.sdlg.setVisible(true);
												dos.writeUTF("request");
												System.out.println("request1");
												fos = new FileOutputStream(f);
												bos = new BufferedOutputStream(fos);
												int fSize = dis.readInt();
												//System.out.println("size : " +fSize);
												double percent = 0;
												String percentStr = null;

												int len;
												int size = 4096;
												byte[] data = new byte[size];
												while ((len = dis.read(data)) != -1) {
													bos.write(data, 0, len);
													percent += len;
													percentStr = String.valueOf((int)(percent / fSize * 100));
													init.sdlg.setLabelText(percentStr);
													//System.out.print("#");
													fSize -= len;
													if(fSize <= 0) break;

												}

												dos.writeUTF("continue");


												bos.flush();
												bos.close();
												fos.close();

												mtime = dt.parse(tmp[2]).getTime();
												f.setLastModified(mtime);

											}
											else{
												dos.writeUTF("exists");
												System.out.println(new Date(f.lastModified()));
												System.out.println(new Date(dt.parse(tmp[2]).getTime()));
												System.out.println("exists");
											}
										}
										else{
											init.frame.setVisible(false);
											//init.sdlg.setVisible(true);
											dos.writeUTF("request");
											System.out.println("request2");
											//System.out.println(f.getPath());
											fos = new FileOutputStream(f);
											bos = new BufferedOutputStream(fos);
											int fSize = dis.readInt();
											//System.out.println("size : " +fSize);
											double percent = 0;
											String percentStr = null;

											int len;
											int size = 4096;
											byte[] data = new byte[size];
											while ((len = dis.read(data)) != -1) {
												bos.write(data, 0, len);
												percent += len;
												percentStr = String.valueOf((int)(percent / fSize * 100));
												init.sdlg.setLabelText(percentStr);
												//System.out.print("#");
												fSize -= len;
												if(fSize <= 0) break;

											}
											dos.writeUTF("continue");


											bos.flush();
											bos.close();
											fos.close();

											mtime = dt.parse(tmp[2]).getTime();
											f.setLastModified(mtime);
										}


									}
									else{

									}

								}
							}
						}
					}
				}
				else{
					break;
				}

			}
			if(cnt != 0){

			}

			//init.sdlg.endReceiveProcess();
		}catch(IOException e){

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


class FileHistory_ReceiveData{				//각각의 파일 수신을 위한 클래스
	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	FileOutputStream fos;
	BufferedOutputStream bos;
	String src;
	FileHistory_ReceiveData(Socket socket, DataInputStream dis, DataOutputStream dos, String project_id, String file_name, String modify_time, String modify_userid){
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;
		try{
			// 파일명을 전송 받고 파일명 수정.


			System.out.println("send message : "+project_id+"&"+file_name+"&"+modify_time+"&"+modify_userid);
			dos.writeUTF(project_id+"&"+file_name+"&"+modify_time+"&"+modify_userid);
			src = Common.src;



			File desti = new File(src+"/temp");
			if(!desti.exists()){
				desti.mkdirs(); 
			}else{

			}
			System.out.println("file name : "+file_name);
			File f = new File(desti+"/"+file_name);
			fos = new FileOutputStream(f);
			bos = new BufferedOutputStream(fos);

			System.out.println(file_name + "create.");

			int fSize = dis.readInt();

			int len;
			int size = 4096;
			byte[] data = new byte[size];
			while ((len = dis.read(data)) != -1) {
				bos.write(data, 0, len);
				fSize -= len;
				if(fSize <= 0) break;

			}

			dos.writeUTF("end");

			bos.flush();
			bos.close();
			fos.close();

			Process p= Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " +f.getAbsolutePath());


			try {
				p.waitFor();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("complete.");
			System.out.println(" size : " + f.length());
		}catch (IOException e){


		}
	}
}

class ReceiveDataForEach{				//각각의 파일 수신을 위한 클래스
	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	FileOutputStream fos;
	BufferedOutputStream bos;
	String src;
	long mtime;
	SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	ReceiveDataForEach(Socket socket, DataInputStream dis, DataOutputStream dos, String file_name, String modify_time, String project_name) {
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;
		try{




			System.out.println("send message : "+file_name);			//project_id & file_name & file_modify_time & project_name & Extensions
			src = Common.src;

			//System.out.println("파일명 " + fName + "을 전송받았습니다.");
			//fName = fName.replaceAll("a", "b");

			// 파일을 생성하고 파일에 대한 출력 스트림 생성


			File desti = new File(src+"/"+Common.loginId);

			if(!desti.exists()){
				//없다면 생성
				desti.mkdirs(); 
			}else{

			}

			System.out.println("file name : "+file_name);
			File f = new File(desti+"/"+project_name+"/"+file_name);
			fos = new FileOutputStream(f);
			bos = new BufferedOutputStream(fos);

			System.out.println(file_name + "create.");

			int fSize = dis.readInt();

			// 바이트 데이터를 전송받으면서 기록
			int len;
			int size = 4096;
			byte[] data = new byte[size];
			while ((len = dis.read(data)) != -1) {
				//if(len < size) break;
				bos.write(data, 0, len);
				//	System.out.println(len);
				fSize -= len;
				if(fSize <= 0) break;

			}

			//bos.write(data, 0, len);

			bos.flush();
			bos.close();
			fos.close();

			try {
				mtime = dt.parse(modify_time).getTime();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			f.setLastModified(mtime);

			System.out.println("complete.");
			System.out.println(" size : " + f.length());




		}catch (IOException e){


		}
	}
}

class ReceiveStart{

	ReceiveStart(Socket socket, DataInputStream dis, DataOutputStream dos, String file_info){


		String[] tmp1 = file_info.split("&&");
		String file_name = "";
		String project_name = "";
		String modify_time = "";
		try {

			for(int i=0;i<tmp1.length;i++){
				String[] tmp2 = tmp1[i].split("&");
				file_name = tmp2[1]+"."+tmp2[4];
				modify_time = tmp2[2];
				project_name = tmp2[3];

				new ReceiveDataForEach(socket, dis, dos, file_name, modify_time, project_name);


				if(i == tmp1.length-1){
					dos.writeUTF("end");
				}
				else{
					dos.writeUTF("continue");
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}


class WorkState {
	static boolean state = false;

	public boolean getState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

}
