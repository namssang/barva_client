package barva_client;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;

public class ProgramConfig extends JDialog{

	JLabel src_title;
	JLabel src;
	JButton ch_src;

	ProgramConfig(){
		//setUndecorated(true);
		setLayout(null);
		Font f1;
		f1 = new Font("견고딕",Font.BOLD,13);
		getContentPane().setBackground( Color.darkGray );
		
		
		src_title = new JLabel("현재 저장 위치");
		src = new JLabel(Common.src);
		ch_src = new JButton("변경");

		src_title.setFont(f1);
		src.setFont(f1);
		ch_src.setFont(f1);
		src_title.setForeground(Color.lightGray);
		ch_src.setForeground(Color.lightGray);
		
		
		ch_src.setFocusable(false);


		src_title.setBounds(10,10,100,30);
		src.setBounds(110,10,250,30);
		ch_src.setBounds(110,40,100,30);
		

		src.setForeground(Color.lightGray);
		ch_src.setContentAreaFilled(false);

		


		add(src_title);
		add(src);
		add(ch_src);


		setLocation(1370,290);
		setSize(400,400);
		setVisible(true);


	}



}
