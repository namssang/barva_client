package barva_client;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

/*===========================================
 회원수정
 ============================================*/

@SuppressWarnings("serial")
public class MemberModify extends JDialog {   //회원수정
	Socket socket;
	DataOutputStream dos;
	DataInputStream dis;
	WorkState STATE;
	JLabel lblId, lblPwd, lblName, lblHp, lblMail, lblDepart;   //아이디, 비밀번호, 이름, 핸드폰, 메일, 부서
	JTextField tfid;
	JTextField tfname, tfphone, tfemail, tfdepart;   //아이디, 이름, 핸드폰, 메일, 부서
	JButton btnSubmit, btnCancel, btnpwcheck;   //제출버튼, 취소버튼, 중복확인버튼

	MemberModify(final Socket socket, final DataOutputStream dos, final DataInputStream dis, final WorkState state){
		super(init.frame, "회원 수정", true);
		this.socket = socket;
		this.dos = dos;
		this.dis = dis;
		this.STATE = state;

		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);   //x버튼이 먹히지않음.
		while(!Common.EnableClick){
			System.out.print("infinite loop..?");
			//click이 가능해지면 빠져나옴.
		}
		try {
			//BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			dos.writeUTF("MM");               //회원수정 메세지 알림

			setTitle("회원수정");
			setLayout(null);

			Color dd= new Color(189,189,189);
			Font f1;
			f1 = new Font("견고딕",Font.BOLD,15);
			getContentPane().setBackground(dd);

			lblId =new JLabel("아이디");
			lblId.setBounds(20, 20, 100, 20);
			lblId.setForeground(Color.black);
			tfid =new JTextField(20);
			tfid.setBounds(120, 20, 150, 20);
			tfid.setBackground(Color.white);

			lblPwd=new JLabel("패스워드");
			lblPwd.setBounds(20, 50, 100, 20);
			lblPwd.setForeground(Color.black);

			btnpwcheck = new JButton("수정");
			btnpwcheck.setBounds(120, 50, 150, 20);
			btnpwcheck.setForeground(Color.black);
			btnpwcheck.setContentAreaFilled(false);

			btnpwcheck.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JButton a = (JButton)e.getSource();
					if(a == btnpwcheck){
						try {
							PwCheck PwCheck = new PwCheck(socket, dos, dis, state);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			});

			lblName=new JLabel("이름");
			lblName.setBounds(20, 80, 100, 20);
			lblName.setForeground(Color.black);

			tfname= new JTextField(20);
			tfname.setBounds(120, 80, 150, 20);
			tfname.setBackground(Color.white);

			lblHp=new JLabel("핸드폰 번호");
			lblHp.setBounds(20, 110, 100 , 20);
			lblHp.setForeground(Color.black);

			tfphone = new JTextField(20);
			tfphone.setBounds(120, 110, 150, 20);
			tfphone.setBackground(Color.white);


			lblMail=new JLabel("이메일");
			lblMail.setBounds(20, 140, 100, 20);
			lblMail.setForeground(Color.black);

			tfemail = new JTextField(20);
			tfemail.setBounds(120, 140, 150, 20);
			tfemail.setBackground(Color.white);


			lblDepart=new JLabel("부서");
			lblDepart.setBounds(20, 170, 100, 20);
			lblDepart.setForeground(Color.black);

			tfdepart = new JTextField(20);
			tfdepart.setBounds(120, 170, 150, 20);
			tfdepart.setBackground(Color.white);


			btnSubmit = new JButton("수정완료");
			btnSubmit.setBounds(60, 220, 90, 30);
			btnSubmit.setForeground(Color.black);
			btnSubmit.setContentAreaFilled(false);

			btnCancel = new JButton("취소");       
			btnCancel.setBounds(160, 220, 90, 30);
			btnCancel.setForeground(Color.black);
			btnCancel.setContentAreaFilled(false);
			add(lblId);
			add(lblPwd);
			add(lblName);
			add(lblHp);
			add(lblMail);
			add(lblDepart);
			add(tfid);
			add(btnpwcheck);
			add(tfname);
			add(tfphone);
			add(tfemail);
			add(tfdepart);
			add(btnSubmit);
			add(btnCancel);         

			btnCancel.addActionListener(new ActionListener() {   //취소버튼
				public void actionPerformed(ActionEvent e) {
					try {
						dos.writeUTF("cancel");   //취소를 하게 되면 취소 메세지를 보낸다.
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					dispose();
				}
			});

			btnSubmit.addActionListener(new ActionListener() {   //제출버튼
				public void actionPerformed(ActionEvent e) {
					//System.out.println("btnSubmit");

					String name = tfname.getText();
					String phone = tfphone.getText();
					String email = tfemail.getText();
					String department = tfdepart.getText();
					String userId = Common.loginId;

					try {
						//System.out.println(userId+"&"+name+"&"+phone+"&"+email+"&"+department);
						dos.writeUTF(userId+"&"+null+"&"+name+"&"+phone+"&"+email+"&"+department);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					STATE.setState(true);
					setVisible(false);
				}
			});

			setSize(320, 300);
			setLocation(450, 220);
			setResizable(false);
			setVisible(true);    

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	class PwCheck extends JDialog {

		JLabel laPwd;   //비밀번호
		JTextField tfPwd;
		JButton btnPwd;   //바꿀 비밀번호 확인버튼

		Socket socket;
		DataOutputStream dos;
		DataInputStream dis;
		WorkState STATE;

		PwCheck(Socket socket, final DataOutputStream dos, final DataInputStream dis, WorkState state) throws IOException{

			//setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);   //x버튼이 먹히지않음.
			Font f1;
			f1 = new Font("견고딕",Font.BOLD,15);
			getContentPane().setBackground( Color.darkGray );

			setTitle("비밀번호 수정");
			setLayout(null);
			setSize(400, 100);
			setLocation(450, 220);
			setResizable(false);
			setVisible(true);
			laPwd = new JLabel("비밀번호 수정");
			laPwd.setBounds(50, 30, 100, 20);
			laPwd.setForeground(Color.black);


			tfPwd = new JTextField(15);
			tfPwd.setBounds(150, 30, 100, 20);
			tfPwd.setBackground(Color.lightGray);

			btnPwd = new JButton("확인");
			btnPwd.setBounds(260, 30, 80, 20);
			btnPwd.setForeground(Color.BLACK);
			btnPwd.setContentAreaFilled(false);
			add(laPwd);
			add(tfPwd);
			add(btnPwd);

			btnPwd.addActionListener(new ActionListener() {   //제출버튼
				public void actionPerformed(ActionEvent e) {
					//System.out.println("pwdSubmit");

					String userId = Common.loginId;
					String password = tfPwd.getText();

					try {
						//System.out.println("check"+"&"+userId+"&"+password);
						dos.writeUTF("check"+"&"+userId+"&"+password);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					setVisible(false);
				}
			});
		}
	}
}