package barva_client;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.tree.DefaultMutableTreeNode;



/*===========================================

로그인 
 ============================================*/


public class LoginProcess extends JDialog{                              //로그인
	Socket socket;
	DataOutputStream dos;
	DataInputStream dis;
	WorkState STATE;
	Font f1;
	JLabel id1 ;
	JTextField loginId ;
	JLabel password1 ;
	JPasswordField loginpassword ;
	JButton okButton ;
	JButton cancelButton ;


	LoginProcess(final Socket socket, final DataOutputStream dos, final DataInputStream dis, WorkState state){
		super(init.frame,"로그인",true);
		this.socket = socket;
		this.dos = dos;
		this.dis = dis;
		this.STATE = state;
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);		//x버튼이 먹히지않음.

		try {
			dos.writeUTF("LOGIN");               //회원가입 메세지 알림
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			Color dd= new Color(189,189,189);

			setTitle("로그인");
			getContentPane().setBackground(dd);

			setLayout(null);

			f1 = new Font("견고딕",Font.BOLD,15);
			id1 = new JLabel("아이디 : ");
			id1.setForeground(Color.black);
			id1.setFont(f1);
			loginId = new JTextField(15);
			loginId.setBackground(Color.white);
			password1 = new JLabel("패스워드 : ");
			password1.setFont(f1);
			password1.setForeground(Color.black);
			loginpassword = new JPasswordField(15);
			loginpassword.setBackground(Color.white);
			okButton = new JButton("확인");
			okButton.setForeground(Color.black);
			okButton.setContentAreaFilled(false);
			okButton.setFont(f1);
			cancelButton = new JButton("취소");
			cancelButton.setForeground(Color.black);
			cancelButton.setContentAreaFilled(false);
			cancelButton.setFont(f1);

			id1.setBounds(10, 30, 100, 20);
			password1.setBounds(10, 90, 100, 20);
			loginId.setBounds(100, 30, 120, 20);
			loginpassword.setBounds(100, 90, 120, 20);
			okButton.setBounds(20, 200, 80, 30);
			cancelButton.setBounds(130, 200, 80, 30);

			add(id1);
			add(loginId);
			add(password1);
			add(loginpassword);
			add(okButton);
			add(cancelButton);



			//다이얼로그 버튼에 액션리스너 붙이기, 이벤트 처리 위함 
			//원래 생성한 객체를 addActionListener()안에 넣어야 하지만 익명중첩클래스사용
			cancelButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						dos.writeUTF("cancel");
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					setVisible(false);

				}
			});
			okButton.addActionListener(
					new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							//System.out.println("okButton");// 다이알로그의 오케이버튼 누르면 창 안보이는 이벤트
							String userId = loginId.getText();
							String password = loginpassword.getText();

							if(password.length()<1 || userId.length() < 1){
								new CommonDialog("정보를 정확하게 입력해주세요");
							}
							else{


								try {
									dos.writeUTF(userId+"&"+password);
									System.out.println("id + password : "+userId+"&"+password);
									if(dis.readBoolean()){
										System.out.println("login complete.");
										Common.loginState = true;
										Common.loginId = userId;
										//init.pjlist.add(Common.loginId);
										//true이면 로그인 성공

										new print_project_list(socket, dos, dis, STATE);

										setVisible(false);
										init.subPanel.setVisible(false);
										init.subPanel2.setVisible(true);
										init.mainPanel.setVisible(false);
										init.loginPanel.setVisible(true);   

										STATE.setState(true);

										while(!Common.startWork){
											System.out.print(".");
										}

										STATE.setState(false);


										dos.writeUTF("AR");
										new ReceiveData(socket, dis, dos);

										STATE.setState(true);

									}
									else{
										System.out.println("login fail\nconfirm your ID or password");
									}
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}



								setVisible(false);
							}
							//                        clientMain_login.isDefaultLookAndFeelDecorated();
							// new clientMain_notlogin(frame);

						}
					}
					);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setSize(250,300);
		setLocation(500, 200);
		setVisible(true);
		setResizable(false);

	}
}

class print_project_list{						

	Socket socket;
	DataOutputStream dos;
	DataInputStream dis;
	WorkState STATE;
	String createMessage;
	String joinMessage;
	String src;
	File  desti;

	print_project_list(Socket socket, DataOutputStream dos, DataInputStream dis, WorkState state){					//아이디만 넘겨받아 관련 프로젝트 목록을 검색, 출력 
		this.socket = socket;
		this.dos = dos;
		this.dis = dis;
		this.STATE = state;

		try {
			createMessage = dis.readUTF();				//개설한 프로젝트 정보를 받아옴.
			joinMessage = dis.readUTF();


			src = Common.src;

			//fName = fName.replaceAll("a", "b");

			// 파일을 생성하고 파일에 대한 출력 스트림 생성

			desti = new File(src+"/"+Common.loginId);
			if(!desti.exists()){
				//없다면 생성
				desti.mkdirs(); 

			}else{

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		src = src + "/" + Common.loginId;

		populateTree(init.pjlist, createMessage, joinMessage);

	}


	public void populateTree(DynamicTree treePanel, String cm, String jm) {


		DefaultMutableTreeNode cp, jp;
		String createProject = new String("개설 프로젝트");
		String joinProject = new String("참여 프로젝트");
		cp = treePanel.addObject(null, createProject);
		jp = treePanel.addObject(null, joinProject);
		FileWriter fw;

		if(!cm.equals("null2")){
			String cResult[] = cm.split("&&");
			for(int i=0; i<cResult.length ; i++){

				String tmp[] = cResult[i].split("&");
				init.pjlist.cpn.add(tmp[0]);								//tmp[0]은 project number
				init.pjlist.cpt.add(tmp[1]);								//tmp[1]은 project title

				desti = new File(src+"/"+tmp[1]);
				if(!desti.exists()){
					//없다면 생성
					desti.mkdirs(); 
					String src = desti.getPath();
					try {
						File f = new File(src+"/"+"info");
						f.createNewFile();
						String comm = "C:\\WINDOWS\\System32\\ATTRIB.EXE +H "+f;
						Runtime.getRuntime().exec(comm);
						fw = new FileWriter(f);
						fw.write(init.pjlist.cpn.get(i));
						fw.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}




					//System.out.println("폴더 생성 완료");
				}else{


				}

				treePanel.addObject(cp, tmp[1]);
			}
			init.pjlist.cph = treePanel.getCPH();

		}
		if(!jm.equals("null2")){
			String jResult[] = jm.split("&&");
			for(int i=0; i<jResult.length ; i++){
				String tmp[] = jResult[i].split("&");
				init.pjlist.jpn.add(tmp[0]);								//tmp[0]은 project number
				init.pjlist.jpt.add(tmp[1]);								//tmp[1]은 project title

				desti = new File(src+"/"+tmp[1]);
				if(!desti.exists()){
					//없다면 생성
					desti.mkdirs(); 
					String src = desti.getPath();
					try {
						File f = new File(src+"/"+"info");
						f.createNewFile();
						String comm = "C:\\WINDOWS\\System32\\ATTRIB.EXE +H "+f;
						Runtime.getRuntime().exec(comm);
						fw = new FileWriter(f);
						fw.write(init.pjlist.jpn.get(i));
						fw.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


					//System.out.println("폴더 생성 완료");
				}else{


				}
				treePanel.addObject(jp, tmp[1]);
			}
			init.pjlist.jph = treePanel.getJPH();

		}

		init.pjlist.pph = treePanel.getPPH();

		//리스너 등록

	}
}