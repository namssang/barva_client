package barva_client;

import java.awt.BorderLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class Common {
	static boolean loginState = false;
	static String loginId = "";
	static boolean EnableClick = true;
	static boolean startWork = false;
	static String src = System.getProperty("user.dir");
	static String configSrc = "";
	static String path = "";
	
	

}

class CommonDialog extends JDialog{

	CommonDialog(String str){
		JOptionPane.showMessageDialog(null, str);
	}

}


class Project {
	
	
	String project_id;
	String projectSubject;
	String pm;
	String regDt;
	String finDt;
	String comment;

	public Project( String[] cpInfo) {
		this.project_id = cpInfo[0];
		this.projectSubject = cpInfo[1];
		this.pm = cpInfo[2];
		SimpleDateFormat df = new SimpleDateFormat("yy-mm-dd");
		SimpleDateFormat ndf = new SimpleDateFormat("yy/mm/dd");
		try {
			Date to = df.parse(cpInfo[3]);
			this.regDt = ndf.format(to);
			to = df.parse(cpInfo[4]);
			this.finDt = ndf.format(to);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.comment = cpInfo[5];
	}
	public String getProjectSubject() {
		return projectSubject;
	}

	public void setProjectSubject(String projectSubject) {
		this.projectSubject = projectSubject;
	}

	public String getPm() {
		return pm;
	}

	public void setPm(String pm) {
		this.pm = pm;
	}

	public String getFinDt() {
		return finDt;
	}

	public void setFinDt(String finDt) {
		this.finDt = finDt;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public String getProject_id(){
		return this.project_id;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public void setProject_id(String project_id) {
		this.project_id = project_id;
	}

	public void print(){
		System.out.println("project_id : "+this.project_id);
		System.out.println("project_subject : "+this.projectSubject);
		System.out.println("project_pm : "+this.pm);
		System.out.println("project_RegDt : "+this.regDt);
		System.out.println("project_FinDt : "+this.finDt);
		System.out.println("project_Comment : "+this.comment);
	
	}

}