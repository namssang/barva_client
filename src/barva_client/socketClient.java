package barva_client;



import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Date;

public class socketClient {


	String serverIp = "localhost";
//	String serverIp = "121.136.230.228";
//	String serverIp = "182.212.193.77";
	Socket socket = null;
	static WorkState STATE = new WorkState(); 
	static DataOutputStream dos;
	static DataInputStream dis;


	socketClient(){



		try {
			// 서버 연결
			socket = new Socket(serverIp, 9999);
			System.out.println("Server Connect success.");
			dos = new DataOutputStream(socket.getOutputStream());
			dis = new DataInputStream(socket.getInputStream());
			//new FileSender(socket, dos, STATE);			//파일 전송 클래스
			CheckMessage cm = new CheckMessage(socket, dos, dis, STATE);
			init.pjlist = new DynamicTree(socket, dos, dis, STATE);				//리스트를 생성.
			cm.start();

		} catch (IOException e) {
			System.out.println("server connect fail.");
			System.out.println("program close.");
			new CommonDialog("서버와의 연결이 실패하였습니다.");
			System.exit(1);
		}


	}


	public Socket getSocket(){
		return this.socket;
	}
	public void setState(boolean state){
		STATE.setState(state);
	}
	public void joinProcess(){
		STATE.setState(false);
		new JoinProcess(socket, dos, dis, STATE);
	}

	public void FileSender(String path){
		//STATE.setState(false);
		//new FileSender(socket, dos, STATE, path);
	}
	public void loginProcess(){

		STATE.setState(false);
		new LoginProcess(socket, dos, dis, STATE);

	}
	public void createProject(){

		STATE.setState(false);
		new CreateProject(socket, dos, dis, STATE);

	}
	public void joinProject(){

		STATE.setState(false);
		new JoinProject(socket, dos, dis, STATE);

	}

	public void MemberModify(){
		STATE.setState(false);
		new MemberModify(socket, dos, dis, STATE);
	}

	public void FileHistory(String file_id, String proejct_id, String file_name){
		STATE.setState(false);
		new FileHistory(socket, dos, dis, STATE, file_id, proejct_id, file_name);
		STATE.setState(true);
	}
	
	public void ProjectReload(){
		STATE.setState(false);
		new ProjectReload(socket, dos, dis, STATE);
		
	}
	
	


	//------------------------------------------

	//------------------------------------------

}



